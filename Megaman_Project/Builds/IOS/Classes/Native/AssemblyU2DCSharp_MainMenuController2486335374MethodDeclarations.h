﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainMenuController
struct MainMenuController_t2486335374;

#include "codegen/il2cpp-codegen.h"

// System.Void MainMenuController::.ctor()
extern "C"  void MainMenuController__ctor_m3036212531 (MainMenuController_t2486335374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenuController::NewGame()
extern "C"  void MainMenuController_NewGame_m3075988597 (MainMenuController_t2486335374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenuController::Stage1()
extern "C"  void MainMenuController_Stage1_m3360614392 (MainMenuController_t2486335374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenuController::Stage2()
extern "C"  void MainMenuController_Stage2_m2937126889 (MainMenuController_t2486335374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenuController::ShowControls()
extern "C"  void MainMenuController_ShowControls_m2927415166 (MainMenuController_t2486335374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenuController::ShowLegal()
extern "C"  void MainMenuController_ShowLegal_m2439175891 (MainMenuController_t2486335374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenuController::Back()
extern "C"  void MainMenuController_Back_m1237001806 (MainMenuController_t2486335374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenuController::MainMenu()
extern "C"  void MainMenuController_MainMenu_m3877301799 (MainMenuController_t2486335374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
