﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// AnimatorControl
struct AnimatorControl_t2500714174;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerController
struct  PlayerController_t4148409433  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 PlayerController::speed
	int32_t ___speed_2;
	// System.Int32 PlayerController::jumpSpeed
	int32_t ___jumpSpeed_3;
	// System.Int32 PlayerController::climbSpeed
	int32_t ___climbSpeed_4;
	// System.Boolean PlayerController::isShooting
	bool ___isShooting_5;
	// UnityEngine.Transform PlayerController::myTransform
	Transform_t3275118058 * ___myTransform_6;
	// UnityEngine.Rigidbody2D PlayerController::rb
	Rigidbody2D_t502193897 * ___rb_7;
	// UnityEngine.AudioSource PlayerController::shootSound
	AudioSource_t1135106623 * ___shootSound_8;
	// System.Single PlayerController::hInput
	float ___hInput_9;
	// UnityEngine.LayerMask PlayerController::playerMask
	LayerMask_t3188175821  ___playerMask_10;
	// AnimatorControl PlayerController::myAnim
	AnimatorControl_t2500714174 * ___myAnim_11;
	// System.Boolean PlayerController::isGrounded
	bool ___isGrounded_12;
	// UnityEngine.Transform PlayerController::groundTag
	Transform_t3275118058 * ___groundTag_13;
	// System.Boolean PlayerController::canClimb
	bool ___canClimb_14;
	// System.Boolean PlayerController::isClimbing
	bool ___isClimbing_15;
	// UnityEngine.Transform PlayerController::firePoint
	Transform_t3275118058 * ___firePoint_16;
	// UnityEngine.GameObject PlayerController::bullet
	GameObject_t1756533147 * ___bullet_17;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___speed_2)); }
	inline int32_t get_speed_2() const { return ___speed_2; }
	inline int32_t* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(int32_t value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_3() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___jumpSpeed_3)); }
	inline int32_t get_jumpSpeed_3() const { return ___jumpSpeed_3; }
	inline int32_t* get_address_of_jumpSpeed_3() { return &___jumpSpeed_3; }
	inline void set_jumpSpeed_3(int32_t value)
	{
		___jumpSpeed_3 = value;
	}

	inline static int32_t get_offset_of_climbSpeed_4() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___climbSpeed_4)); }
	inline int32_t get_climbSpeed_4() const { return ___climbSpeed_4; }
	inline int32_t* get_address_of_climbSpeed_4() { return &___climbSpeed_4; }
	inline void set_climbSpeed_4(int32_t value)
	{
		___climbSpeed_4 = value;
	}

	inline static int32_t get_offset_of_isShooting_5() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___isShooting_5)); }
	inline bool get_isShooting_5() const { return ___isShooting_5; }
	inline bool* get_address_of_isShooting_5() { return &___isShooting_5; }
	inline void set_isShooting_5(bool value)
	{
		___isShooting_5 = value;
	}

	inline static int32_t get_offset_of_myTransform_6() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___myTransform_6)); }
	inline Transform_t3275118058 * get_myTransform_6() const { return ___myTransform_6; }
	inline Transform_t3275118058 ** get_address_of_myTransform_6() { return &___myTransform_6; }
	inline void set_myTransform_6(Transform_t3275118058 * value)
	{
		___myTransform_6 = value;
		Il2CppCodeGenWriteBarrier(&___myTransform_6, value);
	}

	inline static int32_t get_offset_of_rb_7() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___rb_7)); }
	inline Rigidbody2D_t502193897 * get_rb_7() const { return ___rb_7; }
	inline Rigidbody2D_t502193897 ** get_address_of_rb_7() { return &___rb_7; }
	inline void set_rb_7(Rigidbody2D_t502193897 * value)
	{
		___rb_7 = value;
		Il2CppCodeGenWriteBarrier(&___rb_7, value);
	}

	inline static int32_t get_offset_of_shootSound_8() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___shootSound_8)); }
	inline AudioSource_t1135106623 * get_shootSound_8() const { return ___shootSound_8; }
	inline AudioSource_t1135106623 ** get_address_of_shootSound_8() { return &___shootSound_8; }
	inline void set_shootSound_8(AudioSource_t1135106623 * value)
	{
		___shootSound_8 = value;
		Il2CppCodeGenWriteBarrier(&___shootSound_8, value);
	}

	inline static int32_t get_offset_of_hInput_9() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___hInput_9)); }
	inline float get_hInput_9() const { return ___hInput_9; }
	inline float* get_address_of_hInput_9() { return &___hInput_9; }
	inline void set_hInput_9(float value)
	{
		___hInput_9 = value;
	}

	inline static int32_t get_offset_of_playerMask_10() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___playerMask_10)); }
	inline LayerMask_t3188175821  get_playerMask_10() const { return ___playerMask_10; }
	inline LayerMask_t3188175821 * get_address_of_playerMask_10() { return &___playerMask_10; }
	inline void set_playerMask_10(LayerMask_t3188175821  value)
	{
		___playerMask_10 = value;
	}

	inline static int32_t get_offset_of_myAnim_11() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___myAnim_11)); }
	inline AnimatorControl_t2500714174 * get_myAnim_11() const { return ___myAnim_11; }
	inline AnimatorControl_t2500714174 ** get_address_of_myAnim_11() { return &___myAnim_11; }
	inline void set_myAnim_11(AnimatorControl_t2500714174 * value)
	{
		___myAnim_11 = value;
		Il2CppCodeGenWriteBarrier(&___myAnim_11, value);
	}

	inline static int32_t get_offset_of_isGrounded_12() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___isGrounded_12)); }
	inline bool get_isGrounded_12() const { return ___isGrounded_12; }
	inline bool* get_address_of_isGrounded_12() { return &___isGrounded_12; }
	inline void set_isGrounded_12(bool value)
	{
		___isGrounded_12 = value;
	}

	inline static int32_t get_offset_of_groundTag_13() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___groundTag_13)); }
	inline Transform_t3275118058 * get_groundTag_13() const { return ___groundTag_13; }
	inline Transform_t3275118058 ** get_address_of_groundTag_13() { return &___groundTag_13; }
	inline void set_groundTag_13(Transform_t3275118058 * value)
	{
		___groundTag_13 = value;
		Il2CppCodeGenWriteBarrier(&___groundTag_13, value);
	}

	inline static int32_t get_offset_of_canClimb_14() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___canClimb_14)); }
	inline bool get_canClimb_14() const { return ___canClimb_14; }
	inline bool* get_address_of_canClimb_14() { return &___canClimb_14; }
	inline void set_canClimb_14(bool value)
	{
		___canClimb_14 = value;
	}

	inline static int32_t get_offset_of_isClimbing_15() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___isClimbing_15)); }
	inline bool get_isClimbing_15() const { return ___isClimbing_15; }
	inline bool* get_address_of_isClimbing_15() { return &___isClimbing_15; }
	inline void set_isClimbing_15(bool value)
	{
		___isClimbing_15 = value;
	}

	inline static int32_t get_offset_of_firePoint_16() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___firePoint_16)); }
	inline Transform_t3275118058 * get_firePoint_16() const { return ___firePoint_16; }
	inline Transform_t3275118058 ** get_address_of_firePoint_16() { return &___firePoint_16; }
	inline void set_firePoint_16(Transform_t3275118058 * value)
	{
		___firePoint_16 = value;
		Il2CppCodeGenWriteBarrier(&___firePoint_16, value);
	}

	inline static int32_t get_offset_of_bullet_17() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___bullet_17)); }
	inline GameObject_t1756533147 * get_bullet_17() const { return ___bullet_17; }
	inline GameObject_t1756533147 ** get_address_of_bullet_17() { return &___bullet_17; }
	inline void set_bullet_17(GameObject_t1756533147 * value)
	{
		___bullet_17 = value;
		Il2CppCodeGenWriteBarrier(&___bullet_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
