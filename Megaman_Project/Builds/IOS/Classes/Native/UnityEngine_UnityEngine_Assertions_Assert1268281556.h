﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Assertions.Assert
struct  Assert_t1268281556  : public Il2CppObject
{
public:

public:
};

struct Assert_t1268281556_StaticFields
{
public:
	// System.Boolean UnityEngine.Assertions.Assert::raiseExceptions
	bool ___raiseExceptions_0;

public:
	inline static int32_t get_offset_of_raiseExceptions_0() { return static_cast<int32_t>(offsetof(Assert_t1268281556_StaticFields, ___raiseExceptions_0)); }
	inline bool get_raiseExceptions_0() const { return ___raiseExceptions_0; }
	inline bool* get_address_of_raiseExceptions_0() { return &___raiseExceptions_0; }
	inline void set_raiseExceptions_0(bool value)
	{
		___raiseExceptions_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
