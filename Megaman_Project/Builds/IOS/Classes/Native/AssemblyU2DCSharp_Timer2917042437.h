﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Timer
struct  Timer_t2917042437  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Timer::timeLeft
	float ___timeLeft_2;
	// UnityEngine.UI.Text Timer::timerText
	Text_t356221433 * ___timerText_3;

public:
	inline static int32_t get_offset_of_timeLeft_2() { return static_cast<int32_t>(offsetof(Timer_t2917042437, ___timeLeft_2)); }
	inline float get_timeLeft_2() const { return ___timeLeft_2; }
	inline float* get_address_of_timeLeft_2() { return &___timeLeft_2; }
	inline void set_timeLeft_2(float value)
	{
		___timeLeft_2 = value;
	}

	inline static int32_t get_offset_of_timerText_3() { return static_cast<int32_t>(offsetof(Timer_t2917042437, ___timerText_3)); }
	inline Text_t356221433 * get_timerText_3() const { return ___timerText_3; }
	inline Text_t356221433 ** get_address_of_timerText_3() { return &___timerText_3; }
	inline void set_timerText_3(Text_t356221433 * value)
	{
		___timerText_3 = value;
		Il2CppCodeGenWriteBarrier(&___timerText_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
