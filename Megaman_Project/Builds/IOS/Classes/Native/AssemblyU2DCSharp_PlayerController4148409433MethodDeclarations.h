﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerController
struct PlayerController_t4148409433;
// UnityEngine.Collider2D
struct Collider2D_t646061738;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"

// System.Void PlayerController::.ctor()
extern "C"  void PlayerController__ctor_m3280132936 (PlayerController_t4148409433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::Start()
extern "C"  void PlayerController_Start_m3606284888 (PlayerController_t4148409433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::Update()
extern "C"  void PlayerController_Update_m4228472513 (PlayerController_t4148409433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void PlayerController_OnTriggerEnter2D_m429568576 (PlayerController_t4148409433 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void PlayerController_OnTriggerStay2D_m869855551 (PlayerController_t4148409433 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void PlayerController_OnTriggerExit2D_m1507677490 (PlayerController_t4148409433 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::Move(System.Single)
extern "C"  void PlayerController_Move_m3117066216 (PlayerController_t4148409433 * __this, float ___horizontal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::Jump()
extern "C"  void PlayerController_Jump_m563359566 (PlayerController_t4148409433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::Climb()
extern "C"  void PlayerController_Climb_m3696451561 (PlayerController_t4148409433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::StartClimb(System.Single)
extern "C"  void PlayerController_StartClimb_m2708325488 (PlayerController_t4148409433 * __this, float ___inputVal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::Shoot()
extern "C"  void PlayerController_Shoot_m8662559 (PlayerController_t4148409433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::StartMove(System.Single)
extern "C"  void PlayerController_StartMove_m1739939922 (PlayerController_t4148409433 * __this, float ___horizontal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::StopShoot(System.Boolean)
extern "C"  void PlayerController_StopShoot_m1339709834 (PlayerController_t4148409433 * __this, bool ___shoot0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
