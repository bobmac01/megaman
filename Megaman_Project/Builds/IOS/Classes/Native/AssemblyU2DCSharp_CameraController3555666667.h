﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraController
struct  CameraController_t3555666667  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject CameraController::target
	GameObject_t1756533147 * ___target_2;
	// System.Boolean CameraController::bounds
	bool ___bounds_3;
	// UnityEngine.Vector3 CameraController::minPos
	Vector3_t2243707580  ___minPos_4;
	// UnityEngine.Vector3 CameraController::maxPos
	Vector3_t2243707580  ___maxPos_5;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(CameraController_t3555666667, ___target_2)); }
	inline GameObject_t1756533147 * get_target_2() const { return ___target_2; }
	inline GameObject_t1756533147 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(GameObject_t1756533147 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier(&___target_2, value);
	}

	inline static int32_t get_offset_of_bounds_3() { return static_cast<int32_t>(offsetof(CameraController_t3555666667, ___bounds_3)); }
	inline bool get_bounds_3() const { return ___bounds_3; }
	inline bool* get_address_of_bounds_3() { return &___bounds_3; }
	inline void set_bounds_3(bool value)
	{
		___bounds_3 = value;
	}

	inline static int32_t get_offset_of_minPos_4() { return static_cast<int32_t>(offsetof(CameraController_t3555666667, ___minPos_4)); }
	inline Vector3_t2243707580  get_minPos_4() const { return ___minPos_4; }
	inline Vector3_t2243707580 * get_address_of_minPos_4() { return &___minPos_4; }
	inline void set_minPos_4(Vector3_t2243707580  value)
	{
		___minPos_4 = value;
	}

	inline static int32_t get_offset_of_maxPos_5() { return static_cast<int32_t>(offsetof(CameraController_t3555666667, ___maxPos_5)); }
	inline Vector3_t2243707580  get_maxPos_5() const { return ___maxPos_5; }
	inline Vector3_t2243707580 * get_address_of_maxPos_5() { return &___maxPos_5; }
	inline void set_maxPos_5(Vector3_t2243707580  value)
	{
		___maxPos_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
