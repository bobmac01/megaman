﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AnimatorControl
struct AnimatorControl_t2500714174;
// UnityEngine.Animator
struct Animator_t69676727;
// System.Object
struct Il2CppObject;
// CameraController
struct CameraController_t3555666667;
// Enemy
struct Enemy_t1088811588;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// PlayerHealth
struct PlayerHealth_t2894595013;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Enemy/<JumpingLogic>c__Iterator0
struct U3CJumpingLogicU3Ec__Iterator0_t221989874;
// EnemyHealth
struct EnemyHealth_t704393934;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// GameManager
struct GameManager_t2252321495;
// GreenSpinner
struct GreenSpinner_t3572071360;
// GreenSpinner/<WaitShoot>c__Iterator0
struct U3CWaitShootU3Ec__Iterator0_t2318521998;
// GreenSpinnerShoot
struct GreenSpinnerShoot_t3872673347;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;
// HealthPickup
struct HealthPickup_t1380905078;
// Ladder
struct Ladder_t3942361356;
// MainMenuController
struct MainMenuController_t2486335374;
// MovingPlatform
struct MovingPlatform_t2706464437;
// PlayerController
struct PlayerController_t4148409433;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// ScoreManager
struct ScoreManager_t3573108141;
// Shooting
struct Shooting_t3467275689;
// Tiled2Unity.ImportBehaviour
struct ImportBehaviour_t1918973942;
// Tiled2Unity.SortingLayerExposed
struct SortingLayerExposed_t870693243;
// Tiled2Unity.SpriteDepthInMap
struct SpriteDepthInMap_t2395493699;
// Tiled2Unity.TiledMap
struct TiledMap_t4203693682;
// Tiled2Unity.TileAnimator
struct TileAnimator_t1251487515;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// Tiled2Unity.TiledInitialShaderProperties
struct TiledInitialShaderProperties_t1898058332;
// Tiled2Unity.TileObject
struct TileObject_t2436995085;
// Timer
struct Timer_t2917042437;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimatorControl2500714174.h"
#include "AssemblyU2DCSharp_AnimatorControl2500714174MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "AssemblyU2DCSharp_CameraController3555666667.h"
#include "AssemblyU2DCSharp_CameraController3555666667MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "AssemblyU2DCSharp_Enemy1088811588.h"
#include "AssemblyU2DCSharp_Enemy1088811588MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics2D2540166467MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "AssemblyU2DCSharp_PlayerHealth2894595013MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayerHealth2894595013.h"
#include "AssemblyU2DCSharp_Enemy_U3CJumpingLogicU3Ec__Iterat221989874MethodDeclarations.h"
#include "AssemblyU2DCSharp_Enemy_U3CJumpingLogicU3Ec__Iterat221989874.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_EnemyHealth704393934.h"
#include "AssemblyU2DCSharp_EnemyHealth704393934MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager3573108141.h"
#include "AssemblyU2DCSharp_ScoreManager3573108141MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_GameManager2252321495.h"
#include "AssemblyU2DCSharp_GameManager2252321495MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965MethodDeclarations.h"
#include "AssemblyU2DCSharp_GreenSpinner3572071360.h"
#include "AssemblyU2DCSharp_GreenSpinner3572071360MethodDeclarations.h"
#include "AssemblyU2DCSharp_GreenSpinner_U3CWaitShootU3Ec__I2318521998MethodDeclarations.h"
#include "AssemblyU2DCSharp_GreenSpinner_U3CWaitShootU3Ec__I2318521998.h"
#include "AssemblyU2DCSharp_GreenSpinnerShoot3872673347.h"
#include "AssemblyU2DCSharp_GreenSpinnerShoot3872673347MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754MethodDeclarations.h"
#include "AssemblyU2DCSharp_HealthPickup1380905078.h"
#include "AssemblyU2DCSharp_HealthPickup1380905078MethodDeclarations.h"
#include "AssemblyU2DCSharp_Ladder3942361356.h"
#include "AssemblyU2DCSharp_Ladder3942361356MethodDeclarations.h"
#include "AssemblyU2DCSharp_MainMenuController2486335374.h"
#include "AssemblyU2DCSharp_MainMenuController2486335374MethodDeclarations.h"
#include "AssemblyU2DCSharp_MovingPlatform2706464437.h"
#include "AssemblyU2DCSharp_MovingPlatform2706464437MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayerController4148409433.h"
#include "AssemblyU2DCSharp_PlayerController4148409433MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UnityEngine_Handheld4075775256MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "AssemblyU2DCSharp_Shooting3467275689.h"
#include "AssemblyU2DCSharp_Shooting3467275689MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tiled2Unity_ImportBehaviour1918973942.h"
#include "AssemblyU2DCSharp_Tiled2Unity_ImportBehaviour1918973942MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tiled2Unity_SortingLayerExposed870693243.h"
#include "AssemblyU2DCSharp_Tiled2Unity_SortingLayerExposed870693243MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tiled2Unity_SpriteDepthInMap2395493699.h"
#include "AssemblyU2DCSharp_Tiled2Unity_SpriteDepthInMap2395493699MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tiled2Unity_TiledMap4203693682.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_Tiled2Unity_TiledMap_MapStaggerI1842809421.h"
#include "AssemblyU2DCSharp_Tiled2Unity_TiledMap_MapStaggerA1991880926.h"
#include "AssemblyU2DCSharp_Tiled2Unity_TiledMap_MapOrientat2782890834.h"
#include "AssemblyU2DCSharp_Tiled2Unity_TiledMap4203693682MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tiled2Unity_TileAnimator1251487515.h"
#include "AssemblyU2DCSharp_Tiled2Unity_TileAnimator1251487515MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshRenderer1268241104.h"
#include "UnityEngine_UnityEngine_Assertions_Assert1268281556MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tiled2Unity_TiledInitialShaderPr1898058332.h"
#include "AssemblyU2DCSharp_Tiled2Unity_TiledInitialShaderPr1898058332MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Gizmos2256232573MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tiled2Unity_TiledMap_MapOrientat2782890834MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tiled2Unity_TiledMap_MapStaggerA1991880926MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tiled2Unity_TiledMap_MapStaggerI1842809421MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tiled2Unity_TileObject2436995085.h"
#include "AssemblyU2DCSharp_Tiled2Unity_TileObject2436995085MethodDeclarations.h"
#include "AssemblyU2DCSharp_Timer2917042437.h"
#include "AssemblyU2DCSharp_Timer2917042437MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t69676727_m2717502299(__this, method) ((  Animator_t69676727 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, method) ((  Rigidbody2D_t502193897 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t69676727_m475627522(__this, method) ((  Animator_t69676727 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PlayerHealth>()
#define GameObject_GetComponent_TisPlayerHealth_t2894595013_m1131592430(__this, method) ((  PlayerHealth_t2894595013 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3829784634_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3829784634(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3064851704(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m483057723_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisIl2CppObject_m483057723(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m483057723_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<GreenSpinner>()
#define Object_FindObjectOfType_TisGreenSpinner_t3572071360_m769910995(__this /* static, unused */, method) ((  GreenSpinner_t3572071360 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m483057723_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<PlayerHealth>()
#define Object_FindObjectOfType_TisPlayerHealth_t2894595013_m1264219658(__this /* static, unused */, method) ((  PlayerHealth_t2894595013 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m483057723_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Component::GetComponent<AnimatorControl>()
#define Component_GetComponent_TisAnimatorControl_t2500714174_m3959171427(__this, method) ((  AnimatorControl_t2500714174 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, method) ((  AudioSource_t1135106623 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<PlayerHealth>()
#define Component_GetComponent_TisPlayerHealth_t2894595013_m2947238028(__this, method) ((  PlayerHealth_t2894595013 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Object::FindObjectOfType<PlayerController>()
#define Object_FindObjectOfType_TisPlayerController_t4148409433_m1678123848(__this /* static, unused */, method) ((  PlayerController_t4148409433 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m483057723_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.GameObject::GetComponent<EnemyHealth>()
#define GameObject_GetComponent_TisEnemyHealth_t704393934_m3496941849(__this, method) ((  EnemyHealth_t704393934 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t1268241104_m3460404950(__this, method) ((  MeshRenderer_t1268241104 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
#define GameObject_GetComponent_TisMeshRenderer_t1268241104_m3528968632(__this, method) ((  MeshRenderer_t1268241104 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AnimatorControl::.ctor()
extern "C"  void AnimatorControl__ctor_m637738423 (AnimatorControl_t2500714174 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimatorControl::Start()
extern Il2CppClass* AnimatorControl_t2500714174_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const uint32_t AnimatorControl_Start_m1399485763_MetadataUsageId;
extern "C"  void AnimatorControl_Start_m1399485763 (AnimatorControl_t2500714174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimatorControl_Start_m1399485763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_myTransformAnim_3(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Animator_t69676727 * L_2 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_1, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_myAnimAnimator_4(L_2);
		Transform_t3275118058 * L_3 = __this->get_myTransformAnim_3();
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_localScale_m3074381503(L_3, /*hidden argument*/NULL);
		__this->set_artScale_5(L_4);
		((AnimatorControl_t2500714174_StaticFields*)AnimatorControl_t2500714174_il2cpp_TypeInfo_var->static_fields)->set_instance_2(__this);
		return;
	}
}
// System.Void AnimatorControl::FlipPlayer(System.Single)
extern "C"  void AnimatorControl_FlipPlayer_m892024196 (AnimatorControl_t2500714174 * __this, float ___currentspeed0, const MethodInfo* method)
{
	{
		float L_0 = ___currentspeed0;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0021;
		}
	}
	{
		Vector3_t2243707580 * L_1 = __this->get_address_of_artScale_5();
		float L_2 = L_1->get_x_1();
		if ((((float)L_2) == ((float)(8.0f))))
		{
			goto IL_0041;
		}
	}

IL_0021:
	{
		float L_3 = ___currentspeed0;
		if ((!(((float)L_3) > ((float)(0.0f)))))
		{
			goto IL_006b;
		}
	}
	{
		Vector3_t2243707580 * L_4 = __this->get_address_of_artScale_5();
		float L_5 = L_4->get_x_1();
		if ((!(((float)L_5) == ((float)(-8.0f)))))
		{
			goto IL_006b;
		}
	}

IL_0041:
	{
		Vector3_t2243707580 * L_6 = __this->get_address_of_artScale_5();
		Vector3_t2243707580 * L_7 = L_6;
		float L_8 = L_7->get_x_1();
		L_7->set_x_1(((float)((float)L_8*(float)(-1.0f))));
		Transform_t3275118058 * L_9 = __this->get_myTransformAnim_3();
		Vector3_t2243707580  L_10 = __this->get_artScale_5();
		NullCheck(L_9);
		Transform_set_localScale_m2325460848(L_9, L_10, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// System.Void AnimatorControl::UpdateSpeed(System.Single)
extern Il2CppCodeGenString* _stringLiteral338294183;
extern const uint32_t AnimatorControl_UpdateSpeed_m2943783130_MetadataUsageId;
extern "C"  void AnimatorControl_UpdateSpeed_m2943783130 (AnimatorControl_t2500714174 * __this, float ___currentspeed0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimatorControl_UpdateSpeed_m2943783130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = __this->get_myAnimAnimator_4();
		float L_1 = ___currentspeed0;
		NullCheck(L_0);
		Animator_SetFloat_m956158019(L_0, _stringLiteral338294183, L_1, /*hidden argument*/NULL);
		float L_2 = ___currentspeed0;
		AnimatorControl_FlipPlayer_m892024196(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimatorControl::UpdateGrounded(System.Boolean)
extern Il2CppCodeGenString* _stringLiteral3284461968;
extern const uint32_t AnimatorControl_UpdateGrounded_m956117693_MetadataUsageId;
extern "C"  void AnimatorControl_UpdateGrounded_m956117693 (AnimatorControl_t2500714174 * __this, bool ___ground0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimatorControl_UpdateGrounded_m956117693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = __this->get_myAnimAnimator_4();
		bool L_1 = ___ground0;
		NullCheck(L_0);
		Animator_SetBool_m2305662531(L_0, _stringLiteral3284461968, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimatorControl::UpdateClimbing(System.Boolean)
extern Il2CppCodeGenString* _stringLiteral2939294433;
extern const uint32_t AnimatorControl_UpdateClimbing_m1213998286_MetadataUsageId;
extern "C"  void AnimatorControl_UpdateClimbing_m1213998286 (AnimatorControl_t2500714174 * __this, bool ___climb0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimatorControl_UpdateClimbing_m1213998286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = __this->get_myAnimAnimator_4();
		bool L_1 = ___climb0;
		NullCheck(L_0);
		Animator_SetBool_m2305662531(L_0, _stringLiteral2939294433, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimatorControl::UpdateShoot(System.Boolean)
extern Il2CppCodeGenString* _stringLiteral4195354207;
extern const uint32_t AnimatorControl_UpdateShoot_m2394759276_MetadataUsageId;
extern "C"  void AnimatorControl_UpdateShoot_m2394759276 (AnimatorControl_t2500714174 * __this, bool ___shoot0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimatorControl_UpdateShoot_m2394759276_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = __this->get_myAnimAnimator_4();
		bool L_1 = ___shoot0;
		NullCheck(L_0);
		Animator_SetBool_m2305662531(L_0, _stringLiteral4195354207, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraController::.ctor()
extern "C"  void CameraController__ctor_m3279944138 (CameraController_t3555666667 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraController::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1577610127;
extern const uint32_t CameraController_Update_m2439802515_MetadataUsageId;
extern "C"  void CameraController_Update_m2439802515 (CameraController_t3555666667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraController_Update_m2439802515_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		GameObject_t1756533147 * L_0 = __this->get_target_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1577610127, /*hidden argument*/NULL);
		goto IL_0127;
	}

IL_0023:
	{
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_target_2();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = (&V_0)->get_x_1();
		GameObject_t1756533147 * L_7 = __this->get_target_2();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = Transform_get_position_m1104419803(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		float L_10 = (&V_1)->get_y_2();
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t2243707580  L_12 = Transform_get_position_m1104419803(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		float L_13 = (&V_2)->get_z_3();
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, L_6, ((float)((float)L_10+(float)(3.0f))), L_13, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m2469242620(L_2, L_14, /*hidden argument*/NULL);
		bool L_15 = __this->get_bounds_3();
		if (!L_15)
		{
			goto IL_0126;
		}
	}
	{
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t2243707580  L_18 = Transform_get_position_m1104419803(L_17, /*hidden argument*/NULL);
		V_3 = L_18;
		float L_19 = (&V_3)->get_x_1();
		Vector3_t2243707580 * L_20 = __this->get_address_of_minPos_4();
		float L_21 = L_20->get_x_1();
		Vector3_t2243707580 * L_22 = __this->get_address_of_maxPos_5();
		float L_23 = L_22->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_24 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_19, L_21, L_23, /*hidden argument*/NULL);
		Transform_t3275118058 * L_25 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t2243707580  L_26 = Transform_get_position_m1104419803(L_25, /*hidden argument*/NULL);
		V_4 = L_26;
		float L_27 = (&V_4)->get_y_2();
		Vector3_t2243707580 * L_28 = __this->get_address_of_minPos_4();
		float L_29 = L_28->get_y_2();
		Vector3_t2243707580 * L_30 = __this->get_address_of_maxPos_5();
		float L_31 = L_30->get_y_2();
		float L_32 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_27, L_29, L_31, /*hidden argument*/NULL);
		Transform_t3275118058 * L_33 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t2243707580  L_34 = Transform_get_position_m1104419803(L_33, /*hidden argument*/NULL);
		V_5 = L_34;
		float L_35 = (&V_5)->get_z_3();
		Vector3_t2243707580 * L_36 = __this->get_address_of_minPos_4();
		float L_37 = L_36->get_z_3();
		Vector3_t2243707580 * L_38 = __this->get_address_of_maxPos_5();
		float L_39 = L_38->get_z_3();
		float L_40 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_35, L_37, L_39, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Vector3__ctor_m2638739322(&L_41, L_24, L_32, L_40, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_position_m2469242620(L_16, L_41, /*hidden argument*/NULL);
	}

IL_0126:
	{
	}

IL_0127:
	{
		return;
	}
}
// System.Void Enemy::.ctor()
extern "C"  void Enemy__ctor_m2474411757 (Enemy_t1088811588 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Enemy::Start()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1998288776;
extern const uint32_t Enemy_Start_m4271536181_MetadataUsageId;
extern "C"  void Enemy_Start_m4271536181 (Enemy_t1088811588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enemy_Start_m4271536181_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_enemyTrans_3(L_0);
		Rigidbody2D_t502193897 * L_1 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		__this->set_rb_2(L_1);
		Animator_t69676727 * L_2 = Component_GetComponent_TisAnimator_t69676727_m475627522(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		__this->set_myAnim_4(L_2);
		String_t* L_3 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, L_3, _stringLiteral1998288776, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = GameObject_Find_m836511350(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		__this->set_groundTag_13(L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_IgnoreLayerCollision_m3724642713(NULL /*static, unused*/, ((int32_t)10), ((int32_t)10), /*hidden argument*/NULL);
		Il2CppObject * L_7 = Enemy_JumpingLogic_m15362995(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Enemy::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2016212068;
extern Il2CppCodeGenString* _stringLiteral2968523474;
extern const uint32_t Enemy_Update_m1726793138_MetadataUsageId;
extern "C"  void Enemy_Update_m1726793138 (Enemy_t1088811588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enemy_Update_m1726793138_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0054;
		}
	}
	{
		Transform_t3275118058 * L_2 = __this->get_enemyTrans_3();
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = __this->get_groundTag_13();
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_position_m1104419803(L_5, /*hidden argument*/NULL);
		Vector2_t2243707579  L_7 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		LayerMask_t3188175821  L_8 = __this->get_enemyMask_11();
		int32_t L_9 = LayerMask_op_Implicit_m2135076047(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_10 = Physics2D_Linecast_m2374117908(NULL /*static, unused*/, L_4, L_7, L_9, /*hidden argument*/NULL);
		bool L_11 = RaycastHit2D_op_Implicit_m596912073(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		__this->set_isGrounded_12(L_11);
		goto IL_0060;
	}

IL_0054:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2016212068, /*hidden argument*/NULL);
	}

IL_0060:
	{
		bool L_12 = __this->get_isGrounded_12();
		if (!L_12)
		{
			goto IL_007e;
		}
	}
	{
		Animator_t69676727 * L_13 = __this->get_myAnim_4();
		NullCheck(L_13);
		Animator_SetBool_m2305662531(L_13, _stringLiteral2968523474, (bool)0, /*hidden argument*/NULL);
	}

IL_007e:
	{
		bool L_14 = __this->get_moveRight_10();
		if (!L_14)
		{
			goto IL_00d8;
		}
	}
	{
		Rigidbody2D_t502193897 * L_15 = __this->get_rb_2();
		float L_16 = __this->get_moveSpeed_5();
		Rigidbody2D_t502193897 * L_17 = __this->get_rb_2();
		NullCheck(L_17);
		Vector2_t2243707579  L_18 = Rigidbody2D_get_velocity_m3310151195(L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		float L_19 = (&V_0)->get_y_1();
		Vector2_t2243707579  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector2__ctor_m3067419446(&L_20, L_16, L_19, /*hidden argument*/NULL);
		NullCheck(L_15);
		Rigidbody2D_set_velocity_m3592751374(L_15, L_20, /*hidden argument*/NULL);
		Transform_t3275118058 * L_21 = __this->get_enemyTrans_3();
		Vector2_t2243707579  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector2__ctor_m3067419446(&L_22, (-10.0f), (10.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_23 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_localScale_m2325460848(L_21, L_23, /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00d8:
	{
		Rigidbody2D_t502193897 * L_24 = __this->get_rb_2();
		float L_25 = __this->get_moveSpeed_5();
		Rigidbody2D_t502193897 * L_26 = __this->get_rb_2();
		NullCheck(L_26);
		Vector2_t2243707579  L_27 = Rigidbody2D_get_velocity_m3310151195(L_26, /*hidden argument*/NULL);
		V_1 = L_27;
		float L_28 = (&V_1)->get_y_1();
		Vector2_t2243707579  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Vector2__ctor_m3067419446(&L_29, ((-L_25)), L_28, /*hidden argument*/NULL);
		NullCheck(L_24);
		Rigidbody2D_set_velocity_m3592751374(L_24, L_29, /*hidden argument*/NULL);
		Transform_t3275118058 * L_30 = __this->get_enemyTrans_3();
		Vector2_t2243707579  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector2__ctor_m3067419446(&L_31, (10.0f), (10.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_32 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_set_localScale_m2325460848(L_30, L_32, /*hidden argument*/NULL);
	}

IL_0123:
	{
		return;
	}
}
// System.Void Enemy::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerHealth_t2894595013_m1131592430_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral898008114;
extern Il2CppCodeGenString* _stringLiteral1848745259;
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t Enemy_OnTriggerEnter2D_m990861181_MetadataUsageId;
extern "C"  void Enemy_OnTriggerEnter2D_m990861181 (Enemy_t1088811588 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enemy_OnTriggerEnter2D_m990861181_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	PlayerHealth_t2894595013 * V_1 = NULL;
	{
		Collider2D_t646061738 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m1425941094(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral898008114, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		__this->set_moveRight_10((bool)0);
	}

IL_0024:
	{
		Collider2D_t646061738 * L_4 = ___other0;
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = GameObject_get_tag_m1425941094(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, _stringLiteral1848745259, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0047;
		}
	}
	{
		__this->set_moveRight_10((bool)1);
	}

IL_0047:
	{
		Collider2D_t646061738 * L_8 = ___other0;
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = GameObject_get_tag_m1425941094(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_10, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_008b;
		}
	}
	{
		Collider2D_t646061738 * L_12 = ___other0;
		NullCheck(L_12);
		GameObject_t1756533147 * L_13 = Component_get_gameObject_m3105766835(L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		GameObject_t1756533147 * L_14 = V_0;
		NullCheck(L_14);
		PlayerHealth_t2894595013 * L_15 = GameObject_GetComponent_TisPlayerHealth_t2894595013_m1131592430(L_14, /*hidden argument*/GameObject_GetComponent_TisPlayerHealth_t2894595013_m1131592430_MethodInfo_var);
		V_1 = L_15;
		PlayerHealth_t2894595013 * L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_16, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_008a;
		}
	}
	{
		PlayerHealth_t2894595013 * L_18 = V_1;
		int32_t L_19 = __this->get_enemydefect_9();
		NullCheck(L_18);
		PlayerHealth_TakeDamage_m3822532457(L_18, L_19, /*hidden argument*/NULL);
	}

IL_008a:
	{
	}

IL_008b:
	{
		return;
	}
}
// System.Void Enemy::OnTriggerStay2D(UnityEngine.Collider2D)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerHealth_t2894595013_m1131592430_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t Enemy_OnTriggerStay2D_m1909694772_MetadataUsageId;
extern "C"  void Enemy_OnTriggerStay2D_m1909694772 (Enemy_t1088811588 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enemy_OnTriggerStay2D_m1909694772_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	PlayerHealth_t2894595013 * V_1 = NULL;
	{
		Collider2D_t646061738 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m1425941094(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		Collider2D_t646061738 * L_4 = ___other0;
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		GameObject_t1756533147 * L_6 = V_0;
		NullCheck(L_6);
		PlayerHealth_t2894595013 * L_7 = GameObject_GetComponent_TisPlayerHealth_t2894595013_m1131592430(L_6, /*hidden argument*/GameObject_GetComponent_TisPlayerHealth_t2894595013_m1131592430_MethodInfo_var);
		V_1 = L_7;
		PlayerHealth_t2894595013 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_8, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003e;
		}
	}
	{
		PlayerHealth_t2894595013 * L_10 = V_1;
		NullCheck(L_10);
		PlayerHealth_PlayerDrown_m3178492883(L_10, /*hidden argument*/NULL);
	}

IL_003e:
	{
	}

IL_003f:
	{
		return;
	}
}
// System.Void Enemy::MoveEnemy()
extern Il2CppCodeGenString* _stringLiteral2968523474;
extern const uint32_t Enemy_MoveEnemy_m175095160_MetadataUsageId;
extern "C"  void Enemy_MoveEnemy_m175095160 (Enemy_t1088811588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enemy_MoveEnemy_m175095160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_rb_2();
		float L_1 = __this->get_moveSpeed_5();
		float L_2 = __this->get_jumpSpeed_6();
		Vector2_t2243707579  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m3067419446(&L_3, L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody2D_set_velocity_m3592751374(L_0, L_3, /*hidden argument*/NULL);
		__this->set_isGrounded_12((bool)0);
		Animator_t69676727 * L_4 = __this->get_myAnim_4();
		NullCheck(L_4);
		Animator_SetBool_m2305662531(L_4, _stringLiteral2968523474, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Enemy::JumpingLogic()
extern Il2CppClass* U3CJumpingLogicU3Ec__Iterator0_t221989874_il2cpp_TypeInfo_var;
extern const uint32_t Enemy_JumpingLogic_m15362995_MetadataUsageId;
extern "C"  Il2CppObject * Enemy_JumpingLogic_m15362995 (Enemy_t1088811588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enemy_JumpingLogic_m15362995_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CJumpingLogicU3Ec__Iterator0_t221989874 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CJumpingLogicU3Ec__Iterator0_t221989874 * L_0 = (U3CJumpingLogicU3Ec__Iterator0_t221989874 *)il2cpp_codegen_object_new(U3CJumpingLogicU3Ec__Iterator0_t221989874_il2cpp_TypeInfo_var);
		U3CJumpingLogicU3Ec__Iterator0__ctor_m3781887137(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CJumpingLogicU3Ec__Iterator0_t221989874 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CJumpingLogicU3Ec__Iterator0_t221989874 * L_2 = V_0;
		V_1 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		Il2CppObject * L_3 = V_1;
		return L_3;
	}
}
// System.Void Enemy/<JumpingLogic>c__Iterator0::.ctor()
extern "C"  void U3CJumpingLogicU3Ec__Iterator0__ctor_m3781887137 (U3CJumpingLogicU3Ec__Iterator0_t221989874 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Enemy/<JumpingLogic>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CJumpingLogicU3Ec__Iterator0_MoveNext_m2179458059_MetadataUsageId;
extern "C"  bool U3CJumpingLogicU3Ec__Iterator0_MoveNext_m2179458059 (U3CJumpingLogicU3Ec__Iterator0_t221989874 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CJumpingLogicU3Ec__Iterator0_MoveNext_m2179458059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_004e;
		}
	}
	{
		goto IL_0066;
	}

IL_0021:
	{
	}

IL_0022:
	{
		Enemy_t1088811588 * L_2 = __this->get_U24this_0();
		NullCheck(L_2);
		float L_3 = L_2->get_seconds_8();
		WaitForSeconds_t3839502067 * L_4 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		bool L_5 = __this->get_U24disposing_2();
		if (L_5)
		{
			goto IL_0049;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0049:
	{
		goto IL_0068;
	}

IL_004e:
	{
		Enemy_t1088811588 * L_6 = __this->get_U24this_0();
		NullCheck(L_6);
		Enemy_MoveEnemy_m175095160(L_6, /*hidden argument*/NULL);
		goto IL_0022;
	}
	// Dead block : IL_005f: ldarg.0

IL_0066:
	{
		return (bool)0;
	}

IL_0068:
	{
		return (bool)1;
	}
}
// System.Object Enemy/<JumpingLogic>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CJumpingLogicU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4017147243 (U3CJumpingLogicU3Ec__Iterator0_t221989874 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object Enemy/<JumpingLogic>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CJumpingLogicU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3360114851 (U3CJumpingLogicU3Ec__Iterator0_t221989874 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void Enemy/<JumpingLogic>c__Iterator0::Dispose()
extern "C"  void U3CJumpingLogicU3Ec__Iterator0_Dispose_m50255436 (U3CJumpingLogicU3Ec__Iterator0_t221989874 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Enemy/<JumpingLogic>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CJumpingLogicU3Ec__Iterator0_Reset_m322117890_MetadataUsageId;
extern "C"  void U3CJumpingLogicU3Ec__Iterator0_Reset_m322117890 (U3CJumpingLogicU3Ec__Iterator0_t221989874 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CJumpingLogicU3Ec__Iterator0_Reset_m322117890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void EnemyHealth::.ctor()
extern "C"  void EnemyHealth__ctor_m2679466021 (EnemyHealth_t704393934 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyHealth::Start()
extern "C"  void EnemyHealth_Start_m2934489173 (EnemyHealth_t704393934 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_myTrans_4(L_0);
		return;
	}
}
// System.Void EnemyHealth::TakeDamage(System.Int32)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreManager_t3573108141_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1541202342;
extern const uint32_t EnemyHealth_TakeDamage_m3053861170_MetadataUsageId;
extern "C"  void EnemyHealth_TakeDamage_m3053861170 (EnemyHealth_t704393934 * __this, int32_t ___amount0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyHealth_TakeDamage_m3053861170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_currentHealth_2();
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1541202342, /*hidden argument*/NULL);
		int32_t L_1 = ((ScoreManager_t3573108141_StaticFields*)ScoreManager_t3573108141_il2cpp_TypeInfo_var->static_fields)->get_score_2();
		int32_t L_2 = __this->get_score_5();
		((ScoreManager_t3573108141_StaticFields*)ScoreManager_t3573108141_il2cpp_TypeInfo_var->static_fields)->set_score_2(((int32_t)((int32_t)L_1+(int32_t)L_2)));
		GameObject_t1756533147 * L_3 = __this->get_healthDrop_3();
		Transform_t3275118058 * L_4 = __this->get_myTrans_4();
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = __this->get_myTrans_4();
		NullCheck(L_6);
		Quaternion_t4030073918  L_7 = Transform_get_rotation_m1033555130(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_3, L_5, L_7, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		EnemyHealth_ProcessEnemyDeath_m2535131214(__this, /*hidden argument*/NULL);
	}

IL_0053:
	{
		int32_t L_8 = __this->get_currentHealth_2();
		int32_t L_9 = ___amount0;
		__this->set_currentHealth_2(((int32_t)((int32_t)L_8-(int32_t)L_9)));
		return;
	}
}
// System.Void EnemyHealth::ProcessEnemyDeath()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3052249722;
extern const uint32_t EnemyHealth_ProcessEnemyDeath_m2535131214_MetadataUsageId;
extern "C"  void EnemyHealth_ProcessEnemyDeath_m2535131214 (EnemyHealth_t704393934 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyHealth_ProcessEnemyDeath_m2535131214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3052249722, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::.ctor()
extern "C"  void GameManager__ctor_m293624896 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameManager_t2252321495_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1097892631;
extern Il2CppCodeGenString* _stringLiteral3863495299;
extern Il2CppCodeGenString* _stringLiteral1884423134;
extern const uint32_t GameManager_Update_m969954595_MetadataUsageId;
extern "C"  void GameManager_Update_m969954595 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_Update_m969954595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_playerObject_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->set_isDead_3((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1097892631, /*hidden argument*/NULL);
	}

IL_0024:
	{
		bool L_2 = ((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->get_isDead_3();
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		GameManager_DestroyPlayer_m143949559(__this, /*hidden argument*/NULL);
	}

IL_0036:
	{
		bool L_3 = ((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->get_levelCompleted_4();
		if (!L_3)
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3863495299, /*hidden argument*/NULL);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral1884423134, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
// System.Void GameManager::DestroyPlayer()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t GameManager_DestroyPlayer_m143949559_MetadataUsageId;
extern "C"  void GameManager_DestroyPlayer_m143949559 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_DestroyPlayer_m143949559_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_playerObject_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GreenSpinner::.ctor()
extern "C"  void GreenSpinner__ctor_m687158761 (GreenSpinner_t3572071360 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GreenSpinner::Start()
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const uint32_t GreenSpinner_Start_m3664346785_MetadataUsageId;
extern "C"  void GreenSpinner_Start_m3664346785 (GreenSpinner_t3572071360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GreenSpinner_Start_m3664346785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_enemyTrans_3(L_0);
		Rigidbody2D_t502193897 * L_1 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		__this->set_rb_2(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_IgnoreLayerCollision_m3724642713(NULL /*static, unused*/, ((int32_t)10), ((int32_t)10), /*hidden argument*/NULL);
		Il2CppObject * L_2 = GreenSpinner_WaitShoot_m1483057635(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GreenSpinner::Update()
extern "C"  void GreenSpinner_Update_m1608625898 (GreenSpinner_t3572071360 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_directionVal_8();
		GreenSpinner_MoveEnemy_m2147191535(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GreenSpinner::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral898008114;
extern Il2CppCodeGenString* _stringLiteral1848745259;
extern const uint32_t GreenSpinner_OnTriggerEnter2D_m2666232697_MetadataUsageId;
extern "C"  void GreenSpinner_OnTriggerEnter2D_m2666232697 (GreenSpinner_t3572071360 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GreenSpinner_OnTriggerEnter2D_m2666232697_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t646061738 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m1425941094(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral898008114, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		__this->set_directionVal_8(0);
	}

IL_0024:
	{
		Collider2D_t646061738 * L_4 = ___other0;
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = GameObject_get_tag_m1425941094(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, _stringLiteral1848745259, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0047;
		}
	}
	{
		__this->set_directionVal_8(1);
	}

IL_0047:
	{
		return;
	}
}
// System.Void GreenSpinner::MoveEnemy(System.Int32)
extern "C"  void GreenSpinner_MoveEnemy_m2147191535 (GreenSpinner_t3572071360 * __this, int32_t ___direction0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___direction0;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0049;
		}
	}
	{
		Rigidbody2D_t502193897 * L_1 = __this->get_rb_2();
		float L_2 = __this->get_moveSpeed_6();
		Vector2_t2243707579  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m3067419446(&L_3, L_2, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		Rigidbody2D_set_velocity_m3592751374(L_1, L_3, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = __this->get_enemyTrans_3();
		Vector2_t2243707579  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2__ctor_m3067419446(&L_5, (-12.0f), (12.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localScale_m2325460848(L_4, L_6, /*hidden argument*/NULL);
		goto IL_008c;
	}

IL_0049:
	{
		int32_t L_7 = ___direction0;
		if (L_7)
		{
			goto IL_008c;
		}
	}
	{
		Rigidbody2D_t502193897 * L_8 = __this->get_rb_2();
		float L_9 = __this->get_moveSpeed_6();
		Vector2_t2243707579  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector2__ctor_m3067419446(&L_10, ((-L_9)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		Rigidbody2D_set_velocity_m3592751374(L_8, L_10, /*hidden argument*/NULL);
		Transform_t3275118058 * L_11 = __this->get_enemyTrans_3();
		Vector2_t2243707579  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector2__ctor_m3067419446(&L_12, (12.0f), (12.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_localScale_m2325460848(L_11, L_13, /*hidden argument*/NULL);
	}

IL_008c:
	{
		return;
	}
}
// System.Void GreenSpinner::Shoot()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const uint32_t GreenSpinner_Shoot_m1844586608_MetadataUsageId;
extern "C"  void GreenSpinner_Shoot_m1844586608 (GreenSpinner_t3572071360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GreenSpinner_Shoot_m1844586608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_bullet_5();
		Transform_t3275118058 * L_1 = __this->get_firePoint_4();
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = __this->get_firePoint_4();
		NullCheck(L_3);
		Quaternion_t4030073918  L_4 = Transform_get_rotation_m1033555130(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_0, L_2, L_4, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		return;
	}
}
// System.Collections.IEnumerator GreenSpinner::WaitShoot()
extern Il2CppClass* U3CWaitShootU3Ec__Iterator0_t2318521998_il2cpp_TypeInfo_var;
extern const uint32_t GreenSpinner_WaitShoot_m1483057635_MetadataUsageId;
extern "C"  Il2CppObject * GreenSpinner_WaitShoot_m1483057635 (GreenSpinner_t3572071360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GreenSpinner_WaitShoot_m1483057635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CWaitShootU3Ec__Iterator0_t2318521998 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CWaitShootU3Ec__Iterator0_t2318521998 * L_0 = (U3CWaitShootU3Ec__Iterator0_t2318521998 *)il2cpp_codegen_object_new(U3CWaitShootU3Ec__Iterator0_t2318521998_il2cpp_TypeInfo_var);
		U3CWaitShootU3Ec__Iterator0__ctor_m306744893(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitShootU3Ec__Iterator0_t2318521998 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CWaitShootU3Ec__Iterator0_t2318521998 * L_2 = V_0;
		V_1 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		Il2CppObject * L_3 = V_1;
		return L_3;
	}
}
// System.Void GreenSpinner/<WaitShoot>c__Iterator0::.ctor()
extern "C"  void U3CWaitShootU3Ec__Iterator0__ctor_m306744893 (U3CWaitShootU3Ec__Iterator0_t2318521998 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GreenSpinner/<WaitShoot>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitShootU3Ec__Iterator0_MoveNext_m261123167_MetadataUsageId;
extern "C"  bool U3CWaitShootU3Ec__Iterator0_MoveNext_m261123167 (U3CWaitShootU3Ec__Iterator0_t2318521998 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitShootU3Ec__Iterator0_MoveNext_m261123167_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_004e;
		}
	}
	{
		goto IL_0066;
	}

IL_0021:
	{
	}

IL_0022:
	{
		GreenSpinner_t3572071360 * L_2 = __this->get_U24this_0();
		NullCheck(L_2);
		float L_3 = L_2->get_seconds_7();
		WaitForSeconds_t3839502067 * L_4 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		bool L_5 = __this->get_U24disposing_2();
		if (L_5)
		{
			goto IL_0049;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0049:
	{
		goto IL_0068;
	}

IL_004e:
	{
		GreenSpinner_t3572071360 * L_6 = __this->get_U24this_0();
		NullCheck(L_6);
		GreenSpinner_Shoot_m1844586608(L_6, /*hidden argument*/NULL);
		goto IL_0022;
	}
	// Dead block : IL_005f: ldarg.0

IL_0066:
	{
		return (bool)0;
	}

IL_0068:
	{
		return (bool)1;
	}
}
// System.Object GreenSpinner/<WaitShoot>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitShootU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3685701799 (U3CWaitShootU3Ec__Iterator0_t2318521998 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object GreenSpinner/<WaitShoot>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitShootU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3559192559 (U3CWaitShootU3Ec__Iterator0_t2318521998 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void GreenSpinner/<WaitShoot>c__Iterator0::Dispose()
extern "C"  void U3CWaitShootU3Ec__Iterator0_Dispose_m394302096 (U3CWaitShootU3Ec__Iterator0_t2318521998 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void GreenSpinner/<WaitShoot>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitShootU3Ec__Iterator0_Reset_m2880820038_MetadataUsageId;
extern "C"  void U3CWaitShootU3Ec__Iterator0_Reset_m2880820038 (U3CWaitShootU3Ec__Iterator0_t2318521998 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitShootU3Ec__Iterator0_Reset_m2880820038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GreenSpinnerShoot::.ctor()
extern "C"  void GreenSpinnerShoot__ctor_m2941445414 (GreenSpinnerShoot_t3872673347 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GreenSpinnerShoot::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisGreenSpinner_t3572071360_m769910995_MethodInfo_var;
extern const uint32_t GreenSpinnerShoot_Start_m293141642_MetadataUsageId;
extern "C"  void GreenSpinnerShoot_Start_m293141642 (GreenSpinnerShoot_t3872673347 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GreenSpinnerShoot_Start_m293141642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_t502193897 * L_0 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		__this->set_rb_2(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GreenSpinner_t3572071360 * L_1 = Object_FindObjectOfType_TisGreenSpinner_t3572071360_m769910995(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisGreenSpinner_t3572071360_m769910995_MethodInfo_var);
		__this->set_spinner_3(L_1);
		GreenSpinner_t3572071360 * L_2 = __this->get_spinner_3();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_localScale_m3074381503(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_x_1();
		if ((!(((float)L_5) > ((float)(0.0f)))))
		{
			goto IL_0049;
		}
	}
	{
		float L_6 = __this->get_shootSpeed_4();
		__this->set_shootSpeed_4(((-L_6)));
	}

IL_0049:
	{
		return;
	}
}
// System.Void GreenSpinnerShoot::Update()
extern "C"  void GreenSpinnerShoot_Update_m4212651139 (GreenSpinnerShoot_t3872673347 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_rb_2();
		float L_1 = __this->get_shootSpeed_4();
		Rigidbody2D_t502193897 * L_2 = __this->get_rb_2();
		NullCheck(L_2);
		Vector2_t2243707579  L_3 = Rigidbody2D_get_velocity_m3310151195(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&V_0)->get_y_1();
		Vector2_t2243707579  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2__ctor_m3067419446(&L_5, L_1, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody2D_set_velocity_m3592751374(L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GreenSpinnerShoot::OnCollisionEnter2D(UnityEngine.Collision2D)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerHealth_t2894595013_m1131592430_MethodInfo_var;
extern const uint32_t GreenSpinnerShoot_OnCollisionEnter2D_m2476808972_MetadataUsageId;
extern "C"  void GreenSpinnerShoot_OnCollisionEnter2D_m2476808972 (GreenSpinnerShoot_t3872673347 * __this, Collision2D_t1539500754 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GreenSpinnerShoot_OnCollisionEnter2D_m2476808972_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	PlayerHealth_t2894595013 * V_1 = NULL;
	{
		Collision2D_t1539500754 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Collision2D_get_gameObject_m4234358314(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1756533147 * L_2 = V_0;
		NullCheck(L_2);
		PlayerHealth_t2894595013 * L_3 = GameObject_GetComponent_TisPlayerHealth_t2894595013_m1131592430(L_2, /*hidden argument*/GameObject_GetComponent_TisPlayerHealth_t2894595013_m1131592430_MethodInfo_var);
		V_1 = L_3;
		PlayerHealth_t2894595013 * L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		PlayerHealth_t2894595013 * L_6 = V_1;
		int32_t L_7 = __this->get_bulletDamage_5();
		NullCheck(L_6);
		PlayerHealth_TakeDamage_m3822532457(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0029:
	{
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HealthPickup::.ctor()
extern "C"  void HealthPickup__ctor_m3353756487 (HealthPickup_t1380905078 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HealthPickup::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisPlayerHealth_t2894595013_m1264219658_MethodInfo_var;
extern const uint32_t HealthPickup_Start_m2357248323_MetadataUsageId;
extern "C"  void HealthPickup_Start_m2357248323 (HealthPickup_t1380905078 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthPickup_Start_m2357248323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		PlayerHealth_t2894595013 * L_0 = Object_FindObjectOfType_TisPlayerHealth_t2894595013_m1264219658(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisPlayerHealth_t2894595013_m1264219658_MethodInfo_var);
		__this->set_pHealth_3(L_0);
		return;
	}
}
// System.Void HealthPickup::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t HealthPickup_OnTriggerEnter2D_m4026101659_MetadataUsageId;
extern "C"  void HealthPickup_OnTriggerEnter2D_m4026101659 (HealthPickup_t1380905078 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthPickup_OnTriggerEnter2D_m4026101659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t646061738 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m357168014(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		PlayerHealth_t2894595013 * L_3 = __this->get_pHealth_3();
		int32_t L_4 = __this->get_healthToGive_2();
		NullCheck(L_3);
		PlayerHealth_HealthPickup_m528866477(L_3, L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void Ladder::.ctor()
extern "C"  void Ladder__ctor_m1239273833 (Ladder_t3942361356 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ladder::Start()
extern Il2CppClass* Ladder_t3942361356_il2cpp_TypeInfo_var;
extern const uint32_t Ladder_Start_m2760985817_MetadataUsageId;
extern "C"  void Ladder_Start_m2760985817 (Ladder_t3942361356 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ladder_Start_m2760985817_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Ladder_t3942361356_StaticFields*)Ladder_t3942361356_il2cpp_TypeInfo_var->static_fields)->set_instance_2(__this);
		return;
	}
}
// System.Void Ladder::UpdateClimb(System.Boolean)
extern "C"  void Ladder_UpdateClimb_m3328543748 (Ladder_t3942361356 * __this, bool ___canClimb0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MainMenuController::.ctor()
extern "C"  void MainMenuController__ctor_m3036212531 (MainMenuController_t2486335374 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenuController::NewGame()
extern Il2CppCodeGenString* _stringLiteral2747312760;
extern const uint32_t MainMenuController_NewGame_m3075988597_MetadataUsageId;
extern "C"  void MainMenuController_NewGame_m3075988597 (MainMenuController_t2486335374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenuController_NewGame_m3075988597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral2747312760, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenuController::Stage1()
extern Il2CppCodeGenString* _stringLiteral3525384274;
extern const uint32_t MainMenuController_Stage1_m3360614392_MetadataUsageId;
extern "C"  void MainMenuController_Stage1_m3360614392 (MainMenuController_t2486335374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenuController_Stage1_m3360614392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral3525384274, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenuController::Stage2()
extern Il2CppCodeGenString* _stringLiteral2849713559;
extern const uint32_t MainMenuController_Stage2_m2937126889_MetadataUsageId;
extern "C"  void MainMenuController_Stage2_m2937126889 (MainMenuController_t2486335374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenuController_Stage2_m2937126889_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral2849713559, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenuController::ShowControls()
extern Il2CppCodeGenString* _stringLiteral802694704;
extern const uint32_t MainMenuController_ShowControls_m2927415166_MetadataUsageId;
extern "C"  void MainMenuController_ShowControls_m2927415166 (MainMenuController_t2486335374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenuController_ShowControls_m2927415166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral802694704, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenuController::ShowLegal()
extern Il2CppCodeGenString* _stringLiteral2965356009;
extern const uint32_t MainMenuController_ShowLegal_m2439175891_MetadataUsageId;
extern "C"  void MainMenuController_ShowLegal_m2439175891 (MainMenuController_t2486335374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenuController_ShowLegal_m2439175891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral2965356009, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenuController::Back()
extern Il2CppCodeGenString* _stringLiteral442195652;
extern const uint32_t MainMenuController_Back_m1237001806_MetadataUsageId;
extern "C"  void MainMenuController_Back_m1237001806 (MainMenuController_t2486335374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenuController_Back_m1237001806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral442195652, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenuController::MainMenu()
extern Il2CppCodeGenString* _stringLiteral442195652;
extern const uint32_t MainMenuController_MainMenu_m3877301799_MetadataUsageId;
extern "C"  void MainMenuController_MainMenu_m3877301799 (MainMenuController_t2486335374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenuController_MainMenu_m3877301799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral442195652, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MovingPlatform::.ctor()
extern "C"  void MovingPlatform__ctor_m961234664 (MovingPlatform_t2706464437 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MovingPlatform::Start()
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const uint32_t MovingPlatform_Start_m578698656_MetadataUsageId;
extern "C"  void MovingPlatform_Start_m578698656 (MovingPlatform_t2706464437 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MovingPlatform_Start_m578698656_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t502193897 * L_0 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		__this->set_rb_2(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Animator_t69676727 * L_2 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_1, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_myAnim_4(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_platformTrans_3(L_3);
		return;
	}
}
// System.Void MovingPlatform::Update()
extern "C"  void MovingPlatform_Update_m3559182657 (MovingPlatform_t2706464437 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_directionVal_6();
		MovingPlatform_MovePlatform_m3521879507(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MovingPlatform::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1932275407;
extern Il2CppCodeGenString* _stringLiteral3402028520;
extern Il2CppCodeGenString* _stringLiteral2282734941;
extern Il2CppCodeGenString* _stringLiteral2150712457;
extern Il2CppCodeGenString* _stringLiteral4143129237;
extern const uint32_t MovingPlatform_OnTriggerEnter2D_m2041124968_MetadataUsageId;
extern "C"  void MovingPlatform_OnTriggerEnter2D_m2041124968 (MovingPlatform_t2706464437 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MovingPlatform_OnTriggerEnter2D_m2041124968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t646061738 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m1425941094(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral1932275407, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		__this->set_directionVal_6(0);
	}

IL_0024:
	{
		Collider2D_t646061738 * L_4 = ___other0;
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = GameObject_get_tag_m1425941094(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, _stringLiteral3402028520, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0047;
		}
	}
	{
		__this->set_directionVal_6(1);
	}

IL_0047:
	{
		Collider2D_t646061738 * L_8 = ___other0;
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = GameObject_get_tag_m1425941094(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_10, _stringLiteral2282734941, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_007e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2150712457, /*hidden argument*/NULL);
		Animator_t69676727 * L_12 = __this->get_myAnim_4();
		NullCheck(L_12);
		Animator_SetBool_m2305662531(L_12, _stringLiteral4143129237, (bool)1, /*hidden argument*/NULL);
	}

IL_007e:
	{
		return;
	}
}
// System.Void MovingPlatform::OnTriggerExit2D(UnityEngine.Collider2D)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2282734941;
extern Il2CppCodeGenString* _stringLiteral213033631;
extern Il2CppCodeGenString* _stringLiteral4143129237;
extern const uint32_t MovingPlatform_OnTriggerExit2D_m397890506_MetadataUsageId;
extern "C"  void MovingPlatform_OnTriggerExit2D_m397890506 (MovingPlatform_t2706464437 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MovingPlatform_OnTriggerExit2D_m397890506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t646061738 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m1425941094(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral2282734941, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral213033631, /*hidden argument*/NULL);
		Animator_t69676727 * L_4 = __this->get_myAnim_4();
		NullCheck(L_4);
		Animator_SetBool_m2305662531(L_4, _stringLiteral4143129237, (bool)0, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void MovingPlatform::MovePlatform(System.Int32)
extern "C"  void MovingPlatform_MovePlatform_m3521879507 (MovingPlatform_t2706464437 * __this, int32_t ___direction0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___direction0;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_002a;
		}
	}
	{
		Rigidbody2D_t502193897 * L_1 = __this->get_rb_2();
		float L_2 = __this->get_moveSpeed_5();
		Vector2_t2243707579  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m3067419446(&L_3, L_2, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		Rigidbody2D_set_velocity_m3592751374(L_1, L_3, /*hidden argument*/NULL);
		goto IL_0048;
	}

IL_002a:
	{
		Rigidbody2D_t502193897 * L_4 = __this->get_rb_2();
		float L_5 = __this->get_moveSpeed_5();
		Vector2_t2243707579  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m3067419446(&L_6, ((-L_5)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		Rigidbody2D_set_velocity_m3592751374(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0048:
	{
		return;
	}
}
// System.Void PlayerController::.ctor()
extern "C"  void PlayerController__ctor_m3280132936 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	{
		__this->set_speed_2(((int32_t)10));
		__this->set_jumpSpeed_3(((int32_t)10));
		__this->set_climbSpeed_4(5);
		__this->set_isShooting_5((bool)0);
		__this->set_hInput_9((0.0f));
		__this->set_isGrounded_12((bool)0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::Start()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimatorControl_t2500714174_m3959171427_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1998288776;
extern const uint32_t PlayerController_Start_m3606284888_MetadataUsageId;
extern "C"  void PlayerController_Start_m3606284888 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_Start_m3606284888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t502193897 * L_0 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		__this->set_rb_7(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_myTransform_6(L_1);
		String_t* L_2 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, L_2, _stringLiteral1998288776, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = GameObject_Find_m836511350(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		__this->set_groundTag_13(L_5);
		AnimatorControl_t2500714174 * L_6 = Component_GetComponent_TisAnimatorControl_t2500714174_m3959171427(__this, /*hidden argument*/Component_GetComponent_TisAnimatorControl_t2500714174_m3959171427_MethodInfo_var);
		__this->set_myAnim_11(L_6);
		AudioSource_t1135106623 * L_7 = Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var);
		__this->set_shootSound_8(L_7);
		return;
	}
}
// System.Void PlayerController::Update()
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t PlayerController_Update_m4228472513_MetadataUsageId;
extern "C"  void PlayerController_Update_m4228472513 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_Update_m4228472513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = __this->get_myTransform_6();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = __this->get_groundTag_13();
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_position_m1104419803(L_3, /*hidden argument*/NULL);
		Vector2_t2243707579  L_5 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		LayerMask_t3188175821  L_6 = __this->get_playerMask_10();
		int32_t L_7 = LayerMask_op_Implicit_m2135076047(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_8 = Physics2D_Linecast_m2374117908(NULL /*static, unused*/, L_2, L_5, L_7, /*hidden argument*/NULL);
		bool L_9 = RaycastHit2D_op_Implicit_m596912073(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->set_isGrounded_12(L_9);
		AnimatorControl_t2500714174 * L_10 = __this->get_myAnim_11();
		bool L_11 = __this->get_isGrounded_12();
		NullCheck(L_10);
		AnimatorControl_UpdateGrounded_m956117693(L_10, L_11, /*hidden argument*/NULL);
		float L_12 = __this->get_hInput_9();
		PlayerController_Move_m3117066216(__this, L_12, /*hidden argument*/NULL);
		bool L_13 = __this->get_canClimb_14();
		if (!L_13)
		{
			goto IL_0079;
		}
	}
	{
		bool L_14 = __this->get_isClimbing_15();
		if (!L_14)
		{
			goto IL_0078;
		}
	}
	{
		PlayerController_Climb_m3696451561(__this, /*hidden argument*/NULL);
	}

IL_0078:
	{
	}

IL_0079:
	{
		return;
	}
}
// System.Void PlayerController::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameManager_t2252321495_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral375472578;
extern Il2CppCodeGenString* _stringLiteral2229051005;
extern Il2CppCodeGenString* _stringLiteral2245644126;
extern const uint32_t PlayerController_OnTriggerEnter2D_m429568576_MetadataUsageId;
extern "C"  void PlayerController_OnTriggerEnter2D_m429568576 (PlayerController_t4148409433 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_OnTriggerEnter2D_m429568576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Collider2D_t646061738 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m2079638459(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral375472578, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		__this->set_canClimb_14((bool)1);
	}

IL_0024:
	{
		Collider2D_t646061738 * L_4 = ___other0;
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m2079638459(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, _stringLiteral2229051005, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0068;
		}
	}
	{
		Rigidbody2D_t502193897 * L_8 = __this->get_rb_7();
		Rigidbody2D_t502193897 * L_9 = __this->get_rb_7();
		NullCheck(L_9);
		Vector2_t2243707579  L_10 = Rigidbody2D_get_velocity_m3310151195(L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		float L_11 = (&V_0)->get_y_1();
		Vector2_t2243707579  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector2__ctor_m3067419446(&L_12, (0.0f), L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		Rigidbody2D_set_velocity_m3592751374(L_8, L_12, /*hidden argument*/NULL);
	}

IL_0068:
	{
		Collider2D_t646061738 * L_13 = ___other0;
		NullCheck(L_13);
		GameObject_t1756533147 * L_14 = Component_get_gameObject_m3105766835(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m2079638459(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_15, _stringLiteral2245644126, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_008a;
		}
	}
	{
		((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->set_levelCompleted_4((bool)1);
	}

IL_008a:
	{
		return;
	}
}
// System.Void PlayerController::OnTriggerStay2D(UnityEngine.Collider2D)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisPlayerHealth_t2894595013_m2947238028_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3225395995;
extern const uint32_t PlayerController_OnTriggerStay2D_m869855551_MetadataUsageId;
extern "C"  void PlayerController_OnTriggerStay2D_m869855551 (PlayerController_t4148409433 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_OnTriggerStay2D_m869855551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t646061738 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m2079638459(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral3225395995, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		PlayerHealth_t2894595013 * L_4 = Component_GetComponent_TisPlayerHealth_t2894595013_m2947238028(__this, /*hidden argument*/Component_GetComponent_TisPlayerHealth_t2894595013_m2947238028_MethodInfo_var);
		NullCheck(L_4);
		PlayerHealth_PlayerDrown_m3178492883(L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void PlayerController::OnTriggerExit2D(UnityEngine.Collider2D)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral375472578;
extern const uint32_t PlayerController_OnTriggerExit2D_m1507677490_MetadataUsageId;
extern "C"  void PlayerController_OnTriggerExit2D_m1507677490 (PlayerController_t4148409433 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_OnTriggerExit2D_m1507677490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t646061738 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m2079638459(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral375472578, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		__this->set_canClimb_14((bool)0);
		AnimatorControl_t2500714174 * L_4 = __this->get_myAnim_11();
		NullCheck(L_4);
		AnimatorControl_UpdateClimbing_m1213998286(L_4, (bool)0, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void PlayerController::Move(System.Single)
extern "C"  void PlayerController_Move_m3117066216 (PlayerController_t4148409433 * __this, float ___horizontal0, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_rb_7();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = Rigidbody2D_get_velocity_m3310151195(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = ___horizontal0;
		int32_t L_3 = __this->get_speed_2();
		(&V_0)->set_x_0(((float)((float)L_2*(float)(((float)((float)L_3))))));
		Rigidbody2D_t502193897 * L_4 = __this->get_rb_7();
		Vector2_t2243707579  L_5 = V_0;
		NullCheck(L_4);
		Rigidbody2D_set_velocity_m3592751374(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::Jump()
extern "C"  void PlayerController_Jump_m563359566 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isGrounded_12();
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		Rigidbody2D_t502193897 * L_1 = __this->get_rb_7();
		int32_t L_2 = __this->get_jumpSpeed_3();
		Vector2_t2243707579  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m3067419446(&L_3, (0.0f), (((float)((float)L_2))), /*hidden argument*/NULL);
		NullCheck(L_1);
		Rigidbody2D_set_velocity_m3592751374(L_1, L_3, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void PlayerController::Climb()
extern "C"  void PlayerController_Climb_m3696451561 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	{
		AnimatorControl_t2500714174 * L_0 = __this->get_myAnim_11();
		NullCheck(L_0);
		AnimatorControl_UpdateClimbing_m1213998286(L_0, (bool)1, /*hidden argument*/NULL);
		__this->set_isClimbing_15((bool)1);
		Rigidbody2D_t502193897 * L_1 = __this->get_rb_7();
		int32_t L_2 = __this->get_climbSpeed_4();
		Vector2_t2243707579  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m3067419446(&L_3, (0.0f), (((float)((float)L_2))), /*hidden argument*/NULL);
		NullCheck(L_1);
		Rigidbody2D_set_velocity_m3592751374(L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::StartClimb(System.Single)
extern "C"  void PlayerController_StartClimb_m2708325488 (PlayerController_t4148409433 * __this, float ___inputVal0, const MethodInfo* method)
{
	{
		float L_0 = ___inputVal0;
		if ((!(((float)L_0) == ((float)(1.0f)))))
		{
			goto IL_001a;
		}
	}
	{
		__this->set_isClimbing_15((bool)1);
		goto IL_0023;
	}

IL_001a:
	{
		__this->set_isClimbing_15((bool)0);
	}

IL_0023:
	{
		return;
	}
}
// System.Void PlayerController::Shoot()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const uint32_t PlayerController_Shoot_m8662559_MetadataUsageId;
extern "C"  void PlayerController_Shoot_m8662559 (PlayerController_t4148409433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_Shoot_m8662559_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_isShooting_5((bool)1);
		AnimatorControl_t2500714174 * L_0 = __this->get_myAnim_11();
		bool L_1 = __this->get_isShooting_5();
		NullCheck(L_0);
		AnimatorControl_UpdateShoot_m2394759276(L_0, L_1, /*hidden argument*/NULL);
		Handheld_Vibrate_m2337207740(NULL /*static, unused*/, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_2 = __this->get_shootSound_8();
		NullCheck(L_2);
		AudioSource_Play_m353744792(L_2, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_bullet_17();
		Transform_t3275118058 * L_4 = __this->get_firePoint_16();
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = __this->get_firePoint_16();
		NullCheck(L_6);
		Quaternion_t4030073918  L_7 = Transform_get_rotation_m1033555130(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_3, L_5, L_7, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		return;
	}
}
// System.Void PlayerController::StartMove(System.Single)
extern "C"  void PlayerController_StartMove_m1739939922 (PlayerController_t4148409433 * __this, float ___horizontal0, const MethodInfo* method)
{
	{
		float L_0 = ___horizontal0;
		__this->set_hInput_9(L_0);
		AnimatorControl_t2500714174 * L_1 = __this->get_myAnim_11();
		float L_2 = ___horizontal0;
		NullCheck(L_1);
		AnimatorControl_UpdateSpeed_m2943783130(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::StopShoot(System.Boolean)
extern "C"  void PlayerController_StopShoot_m1339709834 (PlayerController_t4148409433 * __this, bool ___shoot0, const MethodInfo* method)
{
	{
		bool L_0 = ___shoot0;
		__this->set_isShooting_5(L_0);
		AnimatorControl_t2500714174 * L_1 = __this->get_myAnim_11();
		bool L_2 = __this->get_isShooting_5();
		NullCheck(L_1);
		AnimatorControl_UpdateShoot_m2394759276(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerHealth::.ctor()
extern "C"  void PlayerHealth__ctor_m24469338 (PlayerHealth_t2894595013 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerHealth::Update()
extern Il2CppClass* PlayerHealth_t2894595013_il2cpp_TypeInfo_var;
extern Il2CppClass* GameManager_t2252321495_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1270167477;
extern const uint32_t PlayerHealth_Update_m682785001_MetadataUsageId;
extern "C"  void PlayerHealth_Update_m682785001 (PlayerHealth_t2894595013 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerHealth_Update_m682785001_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerHealth_t2894595013_il2cpp_TypeInfo_var);
		int32_t L_0 = ((PlayerHealth_t2894595013_StaticFields*)PlayerHealth_t2894595013_il2cpp_TypeInfo_var->static_fields)->get_currentHealth_3();
		if ((((int32_t)L_0) >= ((int32_t)1)))
		{
			goto IL_001e;
		}
	}
	{
		((GameManager_t2252321495_StaticFields*)GameManager_t2252321495_il2cpp_TypeInfo_var->static_fields)->set_isDead_3((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1270167477, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Void PlayerHealth::TakeDamage(System.Int32)
extern Il2CppClass* PlayerHealth_t2894595013_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1187194965;
extern const uint32_t PlayerHealth_TakeDamage_m3822532457_MetadataUsageId;
extern "C"  void PlayerHealth_TakeDamage_m3822532457 (PlayerHealth_t2894595013 * __this, int32_t ___amount0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerHealth_TakeDamage_m3822532457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerHealth_t2894595013_il2cpp_TypeInfo_var);
		int32_t L_0 = ((PlayerHealth_t2894595013_StaticFields*)PlayerHealth_t2894595013_il2cpp_TypeInfo_var->static_fields)->get_currentHealth_3();
		int32_t L_1 = ___amount0;
		((PlayerHealth_t2894595013_StaticFields*)PlayerHealth_t2894595013_il2cpp_TypeInfo_var->static_fields)->set_currentHealth_3(((int32_t)((int32_t)L_0-(int32_t)L_1)));
		int32_t L_2 = ((PlayerHealth_t2894595013_StaticFields*)PlayerHealth_t2894595013_il2cpp_TypeInfo_var->static_fields)->get_currentHealth_3();
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerHealth_t2894595013_il2cpp_TypeInfo_var);
		((PlayerHealth_t2894595013_StaticFields*)PlayerHealth_t2894595013_il2cpp_TypeInfo_var->static_fields)->set_currentHealth_3(0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1187194965, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return;
	}
}
// System.Void PlayerHealth::HealthPickup(System.Int32)
extern Il2CppClass* PlayerHealth_t2894595013_il2cpp_TypeInfo_var;
extern const uint32_t PlayerHealth_HealthPickup_m528866477_MetadataUsageId;
extern "C"  void PlayerHealth_HealthPickup_m528866477 (PlayerHealth_t2894595013 * __this, int32_t ___toGive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerHealth_HealthPickup_m528866477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerHealth_t2894595013_il2cpp_TypeInfo_var);
		int32_t L_0 = ((PlayerHealth_t2894595013_StaticFields*)PlayerHealth_t2894595013_il2cpp_TypeInfo_var->static_fields)->get_currentHealth_3();
		int32_t L_1 = ___toGive0;
		((PlayerHealth_t2894595013_StaticFields*)PlayerHealth_t2894595013_il2cpp_TypeInfo_var->static_fields)->set_currentHealth_3(((int32_t)((int32_t)L_0+(int32_t)L_1)));
		int32_t L_2 = ((PlayerHealth_t2894595013_StaticFields*)PlayerHealth_t2894595013_il2cpp_TypeInfo_var->static_fields)->get_currentHealth_3();
		int32_t L_3 = __this->get_maxHealth_2();
		if ((((int32_t)L_2) <= ((int32_t)L_3)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_4 = __this->get_maxHealth_2();
		IL2CPP_RUNTIME_CLASS_INIT(PlayerHealth_t2894595013_il2cpp_TypeInfo_var);
		((PlayerHealth_t2894595013_StaticFields*)PlayerHealth_t2894595013_il2cpp_TypeInfo_var->static_fields)->set_currentHealth_3(L_4);
	}

IL_002a:
	{
		return;
	}
}
// System.Void PlayerHealth::PlayerDrown()
extern Il2CppClass* PlayerHealth_t2894595013_il2cpp_TypeInfo_var;
extern const uint32_t PlayerHealth_PlayerDrown_m3178492883_MetadataUsageId;
extern "C"  void PlayerHealth_PlayerDrown_m3178492883 (PlayerHealth_t2894595013 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerHealth_PlayerDrown_m3178492883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayerHealth_t2894595013_il2cpp_TypeInfo_var);
		int32_t L_0 = ((PlayerHealth_t2894595013_StaticFields*)PlayerHealth_t2894595013_il2cpp_TypeInfo_var->static_fields)->get_currentHealth_3();
		((PlayerHealth_t2894595013_StaticFields*)PlayerHealth_t2894595013_il2cpp_TypeInfo_var->static_fields)->set_currentHealth_3(((int32_t)((int32_t)L_0-(int32_t)1)));
		return;
	}
}
// System.Void PlayerHealth::.cctor()
extern Il2CppClass* PlayerHealth_t2894595013_il2cpp_TypeInfo_var;
extern const uint32_t PlayerHealth__cctor_m3084359511_MetadataUsageId;
extern "C"  void PlayerHealth__cctor_m3084359511 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerHealth__cctor_m3084359511_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((PlayerHealth_t2894595013_StaticFields*)PlayerHealth_t2894595013_il2cpp_TypeInfo_var->static_fields)->set_currentHealth_3(((int32_t)100));
		return;
	}
}
// System.Void ScoreManager::.ctor()
extern "C"  void ScoreManager__ctor_m1636387560 (ScoreManager_t3573108141 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager::Awake()
extern Il2CppClass* ScoreManager_t3573108141_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Awake_m2265564573_MetadataUsageId;
extern "C"  void ScoreManager_Awake_m2265564573 (ScoreManager_t3573108141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Awake_m2265564573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((ScoreManager_t3573108141_StaticFields*)ScoreManager_t3573108141_il2cpp_TypeInfo_var->static_fields)->set_score_2(0);
		return;
	}
}
// System.Void ScoreManager::Update()
extern Il2CppClass* ScoreManager_t3573108141_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayerHealth_t2894595013_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1756683522;
extern Il2CppCodeGenString* _stringLiteral3796439318;
extern const uint32_t ScoreManager_Update_m1476368025_MetadataUsageId;
extern "C"  void ScoreManager_Update_m1476368025 (ScoreManager_t3573108141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Update_m1476368025_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_scoreText_3();
		int32_t L_1 = ((ScoreManager_t3573108141_StaticFields*)ScoreManager_t3573108141_il2cpp_TypeInfo_var->static_fields)->get_score_2();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1756683522, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		Text_t356221433 * L_5 = __this->get_healthText_4();
		IL2CPP_RUNTIME_CLASS_INIT(PlayerHealth_t2894595013_il2cpp_TypeInfo_var);
		int32_t L_6 = ((PlayerHealth_t2894595013_StaticFields*)PlayerHealth_t2894595013_il2cpp_TypeInfo_var->static_fields)->get_currentHealth_3();
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3796439318, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_9);
		return;
	}
}
// System.Void Shooting::.ctor()
extern "C"  void Shooting__ctor_m2748436320 (Shooting_t3467275689 * __this, const MethodInfo* method)
{
	{
		__this->set_shootSpeed_4((10.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Shooting::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisPlayerController_t4148409433_m1678123848_MethodInfo_var;
extern const uint32_t Shooting_Start_m1677191848_MetadataUsageId;
extern "C"  void Shooting_Start_m1677191848 (Shooting_t3467275689 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Shooting_Start_m1677191848_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_t502193897 * L_0 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		__this->set_rb_3(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		PlayerController_t4148409433 * L_1 = Object_FindObjectOfType_TisPlayerController_t4148409433_m1678123848(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisPlayerController_t4148409433_m1678123848_MethodInfo_var);
		__this->set_player_2(L_1);
		PlayerController_t4148409433 * L_2 = __this->get_player_2();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_localScale_m3074381503(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_x_1();
		if ((!(((float)L_5) < ((float)(0.0f)))))
		{
			goto IL_0049;
		}
	}
	{
		float L_6 = __this->get_shootSpeed_4();
		__this->set_shootSpeed_4(((-L_6)));
	}

IL_0049:
	{
		return;
	}
}
// System.Void Shooting::Update()
extern "C"  void Shooting_Update_m1217253073 (Shooting_t3467275689 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_rb_3();
		float L_1 = __this->get_shootSpeed_4();
		Rigidbody2D_t502193897 * L_2 = __this->get_rb_3();
		NullCheck(L_2);
		Vector2_t2243707579  L_3 = Rigidbody2D_get_velocity_m3310151195(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&V_0)->get_y_1();
		Vector2_t2243707579  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2__ctor_m3067419446(&L_5, L_1, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody2D_set_velocity_m3592751374(L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Shooting::OnCollisionEnter2D(UnityEngine.Collision2D)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisEnemyHealth_t704393934_m3496941849_MethodInfo_var;
extern const uint32_t Shooting_OnCollisionEnter2D_m3649009394_MetadataUsageId;
extern "C"  void Shooting_OnCollisionEnter2D_m3649009394 (Shooting_t3467275689 * __this, Collision2D_t1539500754 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Shooting_OnCollisionEnter2D_m3649009394_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	EnemyHealth_t704393934 * V_1 = NULL;
	{
		Collision2D_t1539500754 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Collision2D_get_gameObject_m4234358314(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1756533147 * L_2 = V_0;
		NullCheck(L_2);
		EnemyHealth_t704393934 * L_3 = GameObject_GetComponent_TisEnemyHealth_t704393934_m3496941849(L_2, /*hidden argument*/GameObject_GetComponent_TisEnemyHealth_t704393934_m3496941849_MethodInfo_var);
		V_1 = L_3;
		EnemyHealth_t704393934 * L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		EnemyHealth_t704393934 * L_6 = V_1;
		int32_t L_7 = __this->get_bulletDamage_5();
		NullCheck(L_6);
		EnemyHealth_TakeDamage_m3053861170(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0029:
	{
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Tiled2Unity.ImportBehaviour::.ctor()
extern Il2CppCodeGenString* _stringLiteral371857150;
extern Il2CppCodeGenString* _stringLiteral3296242414;
extern const uint32_t ImportBehaviour__ctor_m2267099800_MetadataUsageId;
extern "C"  void ImportBehaviour__ctor_m2267099800 (ImportBehaviour_t1918973942 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImportBehaviour__ctor_m2267099800_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_Tiled2UnityXmlPath_2(_stringLiteral371857150);
		__this->set_ExporterTiled2UnityVersion_3(_stringLiteral3296242414);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Tiled2Unity.ImportBehaviour::Update()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1203811359;
extern const uint32_t ImportBehaviour_Update_m280656869_MetadataUsageId;
extern "C"  void ImportBehaviour_Update_m280656869 (ImportBehaviour_t1918973942 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImportBehaviour_Update_m280656869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_Tiled2UnityXmlPath_2();
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m2079638459(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral1203811359, L_0, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Tiled2Unity.SortingLayerExposed::.ctor()
extern "C"  void SortingLayerExposed__ctor_m47081449 (SortingLayerExposed_t870693243 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Tiled2Unity.SpriteDepthInMap::.ctor()
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t SpriteDepthInMap__ctor_m1376091147_MetadataUsageId;
extern "C"  void SpriteDepthInMap__ctor_m1376091147 (SpriteDepthInMap_t2395493699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteDepthInMap__ctor_m1376091147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		__this->set__AttachedMap_2((TiledMap_t4203693682 *)NULL);
		__this->set_PlacedOnLayerIndex_3(0);
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2243707579  L_0 = V_0;
		__this->set_StaggerPositionOffset_4(L_0);
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2243707579  L_1 = V_0;
		__this->set_SeparationPoint1_5(L_1);
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2243707579  L_2 = V_0;
		__this->set_SeparationPoint2_6(L_2);
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2243707579  L_3 = V_0;
		__this->set_SeparationAxis1_7(L_3);
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2243707579  L_4 = V_0;
		__this->set_SeparationAxis2_8(L_4);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Tiled2Unity.TiledMap Tiled2Unity.SpriteDepthInMap::get_AttachedMap()
extern "C"  TiledMap_t4203693682 * SpriteDepthInMap_get_AttachedMap_m1356298740 (SpriteDepthInMap_t2395493699 * __this, const MethodInfo* method)
{
	TiledMap_t4203693682 * V_0 = NULL;
	{
		TiledMap_t4203693682 * L_0 = __this->get__AttachedMap_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		TiledMap_t4203693682 * L_1 = V_0;
		return L_1;
	}
}
// System.Void Tiled2Unity.SpriteDepthInMap::set_AttachedMap(Tiled2Unity.TiledMap)
extern "C"  void SpriteDepthInMap_set_AttachedMap_m892988967 (SpriteDepthInMap_t2395493699 * __this, TiledMap_t4203693682 * ___value0, const MethodInfo* method)
{
	{
		TiledMap_t4203693682 * L_0 = ___value0;
		__this->set__AttachedMap_2(L_0);
		SpriteDepthInMap_AttachedMapChanged_m1886987523(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Tiled2Unity.SpriteDepthInMap::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4120463658;
extern const uint32_t SpriteDepthInMap_Start_m1494404799_MetadataUsageId;
extern "C"  void SpriteDepthInMap_Start_m1494404799 (SpriteDepthInMap_t2395493699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteDepthInMap_Start_m1494404799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TiledMap_t4203693682 * L_0 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral4120463658, ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_0033;
	}

IL_002d:
	{
		SpriteDepthInMap_AttachedMapChanged_m1886987523(__this, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void Tiled2Unity.SpriteDepthInMap::AttachedMapChanged()
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t SpriteDepthInMap_AttachedMapChanged_m1886987523_MetadataUsageId;
extern "C"  void SpriteDepthInMap_AttachedMapChanged_m1886987523 (SpriteDepthInMap_t2395493699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteDepthInMap_AttachedMapChanged_m1886987523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	Vector2_t2243707579  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector2_t2243707579  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector2_t2243707579  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector2_t2243707579  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector2_t2243707579  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector2_t2243707579  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector2_t2243707579  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector2_t2243707579  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Vector2_t2243707579  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector2_t2243707579  V_18;
	memset(&V_18, 0, sizeof(V_18));
	int32_t G_B3_0 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B12_0 = 0;
	{
		TiledMap_t4203693682 * L_0 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = L_0->get_TileWidth_9();
		V_0 = L_1;
		TiledMap_t4203693682 * L_2 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = L_2->get_TileHeight_10();
		V_1 = L_3;
		TiledMap_t4203693682 * L_4 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = L_4->get_StaggerIndex_4();
		V_2 = L_5;
		TiledMap_t4203693682 * L_6 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = L_6->get_StaggerAxis_3();
		V_3 = L_7;
		TiledMap_t4203693682 * L_8 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = L_8->get_Orientation_2();
		V_4 = L_9;
		int32_t L_10 = V_4;
		if ((!(((uint32_t)L_10) == ((uint32_t)3))))
		{
			goto IL_0056;
		}
	}
	{
		TiledMap_t4203693682 * L_11 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = L_11->get_HexSideLength_5();
		G_B3_0 = L_12;
		goto IL_0057;
	}

IL_0056:
	{
		G_B3_0 = 0;
	}

IL_0057:
	{
		V_5 = G_B3_0;
		int32_t L_13 = V_2;
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_0;
		Vector2_t2243707579  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector2__ctor_m3067419446(&L_15, ((float)((float)(((float)((float)L_14)))*(float)(0.5f))), (0.0f), /*hidden argument*/NULL);
		__this->set_StaggerPositionOffset_4(L_15);
		goto IL_0091;
	}

IL_007f:
	{
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_6));
		Vector2_t2243707579  L_16 = V_6;
		__this->set_StaggerPositionOffset_4(L_16);
	}

IL_0091:
	{
		int32_t L_17 = V_3;
		if (L_17)
		{
			goto IL_009e;
		}
	}
	{
		int32_t L_18 = V_5;
		G_B9_0 = L_18;
		goto IL_009f;
	}

IL_009e:
	{
		G_B9_0 = 0;
	}

IL_009f:
	{
		V_7 = G_B9_0;
		int32_t L_19 = V_3;
		if ((!(((uint32_t)L_19) == ((uint32_t)1))))
		{
			goto IL_00af;
		}
	}
	{
		int32_t L_20 = V_5;
		G_B12_0 = L_20;
		goto IL_00b0;
	}

IL_00af:
	{
		G_B12_0 = 0;
	}

IL_00b0:
	{
		V_8 = G_B12_0;
		int32_t L_21 = V_8;
		Vector2__ctor_m3067419446((&V_9), (0.0f), ((float)((float)(((float)((float)((-L_21)))))*(float)(0.5f))), /*hidden argument*/NULL);
		int32_t L_22 = V_7;
		Vector2__ctor_m3067419446((&V_10), ((float)((float)(((float)((float)((-L_22)))))*(float)(0.5f))), (0.0f), /*hidden argument*/NULL);
		int32_t L_23 = V_1;
		Vector2__ctor_m3067419446((&V_11), (0.0f), ((float)((float)(((float)((float)L_23)))*(float)(0.5f))), /*hidden argument*/NULL);
		int32_t L_24 = V_0;
		Vector2__ctor_m3067419446((&V_12), ((float)((float)(((float)((float)L_24)))*(float)(0.5f))), (0.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_25 = V_12;
		Vector2_t2243707579  L_26 = V_10;
		Vector2_t2243707579  L_27 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		V_13 = L_27;
		Vector2_t2243707579  L_28 = V_11;
		Vector2_t2243707579  L_29 = V_9;
		Vector2_t2243707579  L_30 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		V_14 = L_30;
		Vector2_t2243707579  L_31 = V_13;
		int32_t L_32 = V_7;
		Vector2_t2243707579  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector2__ctor_m3067419446(&L_33, (((float)((float)L_32))), (0.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_34 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_31, L_33, /*hidden argument*/NULL);
		V_15 = L_34;
		Vector2_t2243707579  L_35 = V_14;
		int32_t L_36 = V_0;
		Vector2_t2243707579  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Vector2__ctor_m3067419446(&L_37, (((float)((float)L_36))), (0.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_38 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_35, L_37, /*hidden argument*/NULL);
		V_16 = L_38;
		Vector2_t2243707579  L_39 = V_14;
		Vector2_t2243707579  L_40 = V_13;
		Vector2_t2243707579  L_41 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		V_17 = L_41;
		Vector2_t2243707579  L_42 = V_16;
		Vector2_t2243707579  L_43 = V_15;
		Vector2_t2243707579  L_44 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_42, L_43, /*hidden argument*/NULL);
		V_18 = L_44;
		float L_45 = (&V_17)->get_y_1();
		float L_46 = (&V_17)->get_x_0();
		Vector2_t2243707579  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Vector2__ctor_m3067419446(&L_47, L_45, ((-L_46)), /*hidden argument*/NULL);
		__this->set_SeparationAxis1_7(L_47);
		float L_48 = (&V_18)->get_y_1();
		float L_49 = (&V_18)->get_x_0();
		Vector2_t2243707579  L_50;
		memset(&L_50, 0, sizeof(L_50));
		Vector2__ctor_m3067419446(&L_50, ((-L_48)), L_49, /*hidden argument*/NULL);
		__this->set_SeparationAxis2_8(L_50);
		int32_t L_51 = V_8;
		int32_t L_52 = V_7;
		if ((((int32_t)L_51) <= ((int32_t)L_52)))
		{
			goto IL_01c2;
		}
	}
	{
		Vector2_t2243707579  L_53 = V_12;
		__this->set_SeparationPoint1_5(L_53);
		Vector2_t2243707579  L_54 = V_12;
		int32_t L_55 = V_1;
		Vector2_t2243707579  L_56;
		memset(&L_56, 0, sizeof(L_56));
		Vector2__ctor_m3067419446(&L_56, (0.0f), (((float)((float)L_55))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_57 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_54, L_56, /*hidden argument*/NULL);
		__this->set_SeparationPoint2_6(L_57);
		goto IL_01e5;
	}

IL_01c2:
	{
		Vector2_t2243707579  L_58 = V_11;
		__this->set_SeparationPoint1_5(L_58);
		Vector2_t2243707579  L_59 = V_11;
		int32_t L_60 = V_0;
		Vector2_t2243707579  L_61;
		memset(&L_61, 0, sizeof(L_61));
		Vector2__ctor_m3067419446(&L_61, (((float)((float)L_60))), (0.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_62 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_59, L_61, /*hidden argument*/NULL);
		__this->set_SeparationPoint2_6(L_62);
	}

IL_01e5:
	{
		return;
	}
}
// System.Void Tiled2Unity.SpriteDepthInMap::Update()
extern "C"  void SpriteDepthInMap_Update_m719055644 (SpriteDepthInMap_t2395493699 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	float V_2 = 0.0f;
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float G_B3_0 = 0.0f;
	{
		TiledMap_t4203693682 * L_0 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = L_0->get_Orientation_2();
		V_0 = L_1;
		TiledMap_t4203693682 * L_2 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = TiledMap_AreTilesStaggered_m1869245225(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		TiledMap_t4203693682 * L_4 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = L_4->get_TileHeight_10();
		V_2 = (((float)((float)L_5)));
		TiledMap_t4203693682 * L_6 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Rect_t3681755626  L_7 = TiledMap_GetMapRect_m2769672278(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = Transform_get_position_m1104419803(L_9, /*hidden argument*/NULL);
		Vector2_t2243707579  L_11 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Vector2_t2243707579  L_12 = SpriteDepthInMap_FindGridCoordinates_m3393281566(__this, L_11, /*hidden argument*/NULL);
		V_4 = L_12;
		bool L_13 = V_1;
		if (!L_13)
		{
			goto IL_005f;
		}
	}
	{
		G_B3_0 = (0.25f);
		goto IL_0064;
	}

IL_005f:
	{
		G_B3_0 = (0.5f);
	}

IL_0064:
	{
		V_5 = G_B3_0;
		Vector2_t2243707579 * L_14 = (&V_4);
		float L_15 = L_14->get_x_0();
		float L_16 = V_5;
		L_14->set_x_0(((float)((float)L_15+(float)L_16)));
		Vector2_t2243707579 * L_17 = (&V_4);
		float L_18 = L_17->get_y_1();
		float L_19 = V_5;
		L_17->set_y_1(((float)((float)L_18+(float)L_19)));
		float L_20 = (&V_4)->get_y_1();
		float L_21 = V_2;
		V_6 = ((float)((float)L_20*(float)L_21));
		int32_t L_22 = V_0;
		if ((!(((uint32_t)L_22) == ((uint32_t)1))))
		{
			goto IL_00b3;
		}
	}
	{
		float L_23 = (&V_4)->get_x_0();
		float L_24 = (&V_4)->get_y_1();
		float L_25 = V_2;
		V_6 = ((float)((float)((float)((float)((float)((float)L_23+(float)L_24))*(float)L_25))*(float)(0.5f)));
	}

IL_00b3:
	{
		float L_26 = V_6;
		float L_27 = Rect_get_height_m3128694305((&V_3), /*hidden argument*/NULL);
		V_7 = ((float)((float)((float)((float)L_26/(float)L_27))*(float)(-1.0f)));
		float L_28 = V_2;
		float L_29 = Rect_get_height_m3128694305((&V_3), /*hidden argument*/NULL);
		V_8 = ((float)((float)((float)((float)L_28/(float)L_29))*(float)(-1.0f)));
		float L_30 = V_7;
		float L_31 = V_8;
		int32_t L_32 = __this->get_PlacedOnLayerIndex_3();
		V_7 = ((float)((float)L_30+(float)((float)((float)L_31*(float)(((float)((float)L_32)))))));
		GameObject_t1756533147 * L_33 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_t3275118058 * L_34 = GameObject_get_transform_m909382139(L_33, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_35 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_t3275118058 * L_36 = GameObject_get_transform_m909382139(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_t2243707580  L_37 = Transform_get_position_m1104419803(L_36, /*hidden argument*/NULL);
		V_9 = L_37;
		float L_38 = (&V_9)->get_x_1();
		GameObject_t1756533147 * L_39 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_39);
		Transform_t3275118058 * L_40 = GameObject_get_transform_m909382139(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		Vector3_t2243707580  L_41 = Transform_get_position_m1104419803(L_40, /*hidden argument*/NULL);
		V_10 = L_41;
		float L_42 = (&V_10)->get_y_2();
		float L_43 = V_7;
		Vector3_t2243707580  L_44;
		memset(&L_44, 0, sizeof(L_44));
		Vector3__ctor_m2638739322(&L_44, L_38, L_42, L_43, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_set_position_m2469242620(L_34, L_44, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 Tiled2Unity.SpriteDepthInMap::FindGridCoordinates(UnityEngine.Vector2)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* MapOrientation_t2782890834_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral851180529;
extern const uint32_t SpriteDepthInMap_FindGridCoordinates_m3393281566_MetadataUsageId;
extern "C"  Vector2_t2243707579  SpriteDepthInMap_FindGridCoordinates_m3393281566 (SpriteDepthInMap_t2395493699 * __this, Vector2_t2243707579  ___position0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteDepthInMap_FindGridCoordinates_m3393281566_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector2_t2243707579  L_0 = ___position0;
		TiledMap_t4203693682 * L_1 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Vector2_t2243707579  L_5 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_0, L_4, /*hidden argument*/NULL);
		___position0 = L_5;
		Vector2_t2243707579  L_6 = ___position0;
		TiledMap_t4203693682 * L_7 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		float L_8 = L_7->get_ExportScale_11();
		Vector2_t2243707579  L_9 = Vector2_op_Division_m96580069(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		___position0 = L_9;
		float L_10 = (&___position0)->get_y_1();
		(&___position0)->set_y_1(((-L_10)));
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_1));
		Vector2_t2243707579  L_11 = V_1;
		V_0 = L_11;
		TiledMap_t4203693682 * L_12 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = L_12->get_Orientation_2();
		V_2 = L_13;
		int32_t L_14 = V_2;
		if (L_14)
		{
			goto IL_00a3;
		}
	}
	{
		float L_15 = (&___position0)->get_x_0();
		TiledMap_t4203693682 * L_16 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = L_16->get_TileWidth_9();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_18 = floorf(((float)((float)L_15/(float)(((float)((float)L_17))))));
		(&V_0)->set_x_0(L_18);
		float L_19 = (&___position0)->get_y_1();
		TiledMap_t4203693682 * L_20 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		int32_t L_21 = L_20->get_TileHeight_10();
		float L_22 = floorf(((float)((float)L_19/(float)(((float)((float)L_21))))));
		(&V_0)->set_y_1(L_22);
		goto IL_00ed;
	}

IL_00a3:
	{
		int32_t L_23 = V_2;
		if ((!(((uint32_t)L_23) == ((uint32_t)1))))
		{
			goto IL_00b9;
		}
	}
	{
		Vector2_t2243707579  L_24 = ___position0;
		Vector2_t2243707579  L_25 = SpriteDepthInMap_ScreenToIsometric_m4228889462(__this, L_24, /*hidden argument*/NULL);
		V_0 = L_25;
		goto IL_00ed;
	}

IL_00b9:
	{
		int32_t L_26 = V_2;
		if ((((int32_t)L_26) == ((int32_t)2)))
		{
			goto IL_00c7;
		}
	}
	{
		int32_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)3))))
		{
			goto IL_00d6;
		}
	}

IL_00c7:
	{
		Vector2_t2243707579  L_28 = ___position0;
		Vector2_t2243707579  L_29 = SpriteDepthInMap_ScreenToStaggered_m4236816887(__this, L_28, /*hidden argument*/NULL);
		V_0 = L_29;
		goto IL_00ed;
	}

IL_00d6:
	{
		int32_t L_30 = V_2;
		int32_t L_31 = L_30;
		Il2CppObject * L_32 = Box(MapOrientation_t2782890834_il2cpp_TypeInfo_var, &L_31);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral851180529, L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
	}

IL_00ed:
	{
		Vector2_t2243707579  L_34 = V_0;
		V_3 = L_34;
		goto IL_00f4;
	}

IL_00f4:
	{
		Vector2_t2243707579  L_35 = V_3;
		return L_35;
	}
}
// UnityEngine.Vector2 Tiled2Unity.SpriteDepthInMap::ScreenToIsometric(UnityEngine.Vector2)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t SpriteDepthInMap_ScreenToIsometric_m4228889462_MetadataUsageId;
extern "C"  Vector2_t2243707579  SpriteDepthInMap_ScreenToIsometric_m4228889462 (SpriteDepthInMap_t2395493699 * __this, Vector2_t2243707579  ___position0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteDepthInMap_ScreenToIsometric_m4228889462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2243707579  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		float L_0 = (&___position0)->get_x_0();
		V_0 = L_0;
		float L_1 = (&___position0)->get_y_1();
		V_1 = L_1;
		TiledMap_t4203693682 * L_2 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = L_2->get_TileWidth_9();
		V_2 = L_3;
		TiledMap_t4203693682 * L_4 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = L_4->get_TileHeight_10();
		V_3 = L_5;
		float L_6 = V_0;
		TiledMap_t4203693682 * L_7 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = L_7->get_NumTilesHigh_8();
		int32_t L_9 = V_2;
		V_0 = ((float)((float)L_6-(float)(((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_8*(int32_t)L_9))/(int32_t)2)))))));
		float L_10 = V_1;
		int32_t L_11 = V_3;
		V_4 = ((float)((float)L_10/(float)(((float)((float)L_11)))));
		float L_12 = V_0;
		int32_t L_13 = V_2;
		V_5 = ((float)((float)L_12/(float)(((float)((float)L_13)))));
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_6));
		float L_14 = V_4;
		float L_15 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_16 = floorf(((float)((float)L_14+(float)L_15)));
		(&V_6)->set_x_0(L_16);
		float L_17 = V_4;
		float L_18 = V_5;
		float L_19 = floorf(((float)((float)L_17-(float)L_18)));
		(&V_6)->set_y_1(L_19);
		Vector2_t2243707579  L_20 = V_6;
		V_7 = L_20;
		goto IL_007b;
	}

IL_007b:
	{
		Vector2_t2243707579  L_21 = V_7;
		return L_21;
	}
}
// UnityEngine.Vector2 Tiled2Unity.SpriteDepthInMap::ScreenToStaggered(UnityEngine.Vector2)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t SpriteDepthInMap_ScreenToStaggered_m4236816887_MetadataUsageId;
extern "C"  Vector2_t2243707579  SpriteDepthInMap_ScreenToStaggered_m4236816887 (SpriteDepthInMap_t2395493699 * __this, Vector2_t2243707579  ___position0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteDepthInMap_ScreenToStaggered_m4236816887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2243707579  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector2_t2243707579  V_8;
	memset(&V_8, 0, sizeof(V_8));
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	Vector2_t2243707579  V_14;
	memset(&V_14, 0, sizeof(V_14));
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	float V_19 = 0.0f;
	{
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_1));
		Vector2_t2243707579  L_0 = V_1;
		V_0 = L_0;
		Vector2_t2243707579  L_1 = ___position0;
		Vector2_t2243707579  L_2 = __this->get_StaggerPositionOffset_4();
		Vector2_t2243707579  L_3 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		___position0 = L_3;
		float L_4 = (&___position0)->get_x_0();
		TiledMap_t4203693682 * L_5 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = L_5->get_TileWidth_9();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_4/(float)(((float)((float)L_6))))));
		(&V_0)->set_x_0(L_7);
		float L_8 = (&___position0)->get_y_1();
		TiledMap_t4203693682 * L_9 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = L_9->get_TileHeight_10();
		float L_11 = floorf(((float)((float)L_8/(float)(((float)((float)L_10))))));
		(&V_0)->set_y_1(L_11);
		TiledMap_t4203693682 * L_12 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = L_12->get_TileWidth_9();
		V_2 = (((float)((float)L_13)));
		TiledMap_t4203693682 * L_14 = SpriteDepthInMap_get_AttachedMap_m1356298740(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = L_14->get_TileHeight_10();
		V_3 = (((float)((float)L_15)));
		float L_16 = (&V_0)->get_x_0();
		float L_17 = V_2;
		V_4 = ((float)((float)L_16*(float)L_17));
		float L_18 = (&V_0)->get_y_1();
		float L_19 = V_3;
		V_5 = ((float)((float)L_18*(float)L_19));
		float L_20 = V_4;
		float L_21 = V_5;
		Vector2__ctor_m3067419446((&V_6), L_20, L_21, /*hidden argument*/NULL);
		Vector2_t2243707579  L_22 = __this->get_SeparationPoint1_5();
		Vector2_t2243707579  L_23 = V_6;
		Vector2_t2243707579  L_24 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Vector2_t2243707579  L_25 = __this->get_SeparationPoint2_6();
		Vector2_t2243707579  L_26 = V_6;
		Vector2_t2243707579  L_27 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		V_8 = L_27;
		Vector2_t2243707579  L_28 = __this->get_SeparationAxis1_7();
		Vector2_t2243707579  L_29 = V_7;
		float L_30 = Vector2_Dot_m778921987(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		V_9 = L_30;
		Vector2_t2243707579  L_31 = __this->get_SeparationAxis1_7();
		Vector2_t2243707579  L_32 = V_8;
		float L_33 = Vector2_Dot_m778921987(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		V_10 = L_33;
		float L_34 = V_9;
		float L_35 = V_10;
		float L_36 = Mathf_Min_m1648492575(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		V_11 = L_36;
		float L_37 = V_9;
		float L_38 = V_10;
		float L_39 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		V_12 = L_39;
		Vector2_t2243707579  L_40 = __this->get_SeparationAxis1_7();
		Vector2_t2243707579  L_41 = ___position0;
		float L_42 = Vector2_Dot_m778921987(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		V_13 = L_42;
		float L_43 = V_13;
		float L_44 = V_11;
		if ((!(((float)L_43) < ((float)L_44))))
		{
			goto IL_012d;
		}
	}
	{
		Vector2_t2243707579 * L_45 = (&V_0);
		float L_46 = L_45->get_x_0();
		L_45->set_x_0(((float)((float)L_46-(float)(0.5f))));
		Vector2_t2243707579 * L_47 = (&V_0);
		float L_48 = L_47->get_y_1();
		L_47->set_y_1(((float)((float)L_48-(float)(0.5f))));
		Vector2_t2243707579  L_49 = V_0;
		V_14 = L_49;
		goto IL_0222;
	}

IL_012d:
	{
		float L_50 = V_13;
		float L_51 = V_12;
		if ((!(((float)L_50) > ((float)L_51))))
		{
			goto IL_0165;
		}
	}
	{
		Vector2_t2243707579 * L_52 = (&V_0);
		float L_53 = L_52->get_x_0();
		L_52->set_x_0(((float)((float)L_53+(float)(0.5f))));
		Vector2_t2243707579 * L_54 = (&V_0);
		float L_55 = L_54->get_y_1();
		L_54->set_y_1(((float)((float)L_55+(float)(0.5f))));
		Vector2_t2243707579  L_56 = V_0;
		V_14 = L_56;
		goto IL_0222;
	}

IL_0165:
	{
		Vector2_t2243707579  L_57 = __this->get_SeparationAxis2_8();
		Vector2_t2243707579  L_58 = V_7;
		float L_59 = Vector2_Dot_m778921987(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
		V_15 = L_59;
		Vector2_t2243707579  L_60 = __this->get_SeparationAxis2_8();
		Vector2_t2243707579  L_61 = V_8;
		float L_62 = Vector2_Dot_m778921987(NULL /*static, unused*/, L_60, L_61, /*hidden argument*/NULL);
		V_16 = L_62;
		float L_63 = V_15;
		float L_64 = V_16;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_65 = Mathf_Min_m1648492575(NULL /*static, unused*/, L_63, L_64, /*hidden argument*/NULL);
		V_17 = L_65;
		float L_66 = V_15;
		float L_67 = V_16;
		float L_68 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_66, L_67, /*hidden argument*/NULL);
		V_18 = L_68;
		Vector2_t2243707579  L_69 = __this->get_SeparationAxis2_8();
		Vector2_t2243707579  L_70 = ___position0;
		float L_71 = Vector2_Dot_m778921987(NULL /*static, unused*/, L_69, L_70, /*hidden argument*/NULL);
		V_19 = L_71;
		float L_72 = V_19;
		float L_73 = V_17;
		if ((!(((float)L_72) < ((float)L_73))))
		{
			goto IL_01e1;
		}
	}
	{
		Vector2_t2243707579 * L_74 = (&V_0);
		float L_75 = L_74->get_x_0();
		L_74->set_x_0(((float)((float)L_75+(float)(0.5f))));
		Vector2_t2243707579 * L_76 = (&V_0);
		float L_77 = L_76->get_y_1();
		L_76->set_y_1(((float)((float)L_77-(float)(0.5f))));
		Vector2_t2243707579  L_78 = V_0;
		V_14 = L_78;
		goto IL_0222;
	}

IL_01e1:
	{
		float L_79 = V_19;
		float L_80 = V_18;
		if ((!(((float)L_79) > ((float)L_80))))
		{
			goto IL_0219;
		}
	}
	{
		Vector2_t2243707579 * L_81 = (&V_0);
		float L_82 = L_81->get_x_0();
		L_81->set_x_0(((float)((float)L_82-(float)(0.5f))));
		Vector2_t2243707579 * L_83 = (&V_0);
		float L_84 = L_83->get_y_1();
		L_83->set_y_1(((float)((float)L_84+(float)(0.5f))));
		Vector2_t2243707579  L_85 = V_0;
		V_14 = L_85;
		goto IL_0222;
	}

IL_0219:
	{
		Vector2_t2243707579  L_86 = V_0;
		V_14 = L_86;
		goto IL_0222;
	}

IL_0222:
	{
		Vector2_t2243707579  L_87 = V_14;
		return L_87;
	}
}
// System.Void Tiled2Unity.TileAnimator::.ctor()
extern "C"  void TileAnimator__ctor_m145946291 (TileAnimator_t1251487515 * __this, const MethodInfo* method)
{
	{
		__this->set_StartTime_2((-1.0f));
		__this->set_Duration_3((-1.0f));
		__this->set_TotalAnimationTime_4((-1.0f));
		__this->set_timer_5((0.0f));
		__this->set_meshRenderer_6((MeshRenderer_t1268241104 *)NULL);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Tiled2Unity.TileAnimator::Awake()
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t1268241104_m3460404950_MethodInfo_var;
extern const uint32_t TileAnimator_Awake_m2555760918_MetadataUsageId;
extern "C"  void TileAnimator_Awake_m2555760918 (TileAnimator_t1251487515 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TileAnimator_Awake_m2555760918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MeshRenderer_t1268241104 * L_0 = Component_GetComponent_TisMeshRenderer_t1268241104_m3460404950(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t1268241104_m3460404950_MethodInfo_var);
		__this->set_meshRenderer_6(L_0);
		return;
	}
}
// System.Void Tiled2Unity.TileAnimator::Start()
extern Il2CppCodeGenString* _stringLiteral1107009114;
extern Il2CppCodeGenString* _stringLiteral182548436;
extern Il2CppCodeGenString* _stringLiteral120387999;
extern const uint32_t TileAnimator_Start_m190025111_MetadataUsageId;
extern "C"  void TileAnimator_Start_m190025111 (TileAnimator_t1251487515 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TileAnimator_Start_m190025111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_StartTime_2();
		Assert_IsTrue_m2900904701(NULL /*static, unused*/, (bool)((((int32_t)((!(((float)L_0) >= ((float)(0.0f))))? 1 : 0)) == ((int32_t)0))? 1 : 0), _stringLiteral1107009114, /*hidden argument*/NULL);
		float L_1 = __this->get_Duration_3();
		Assert_IsTrue_m2900904701(NULL /*static, unused*/, (bool)((((float)L_1) > ((float)(0.0f)))? 1 : 0), _stringLiteral182548436, /*hidden argument*/NULL);
		float L_2 = __this->get_TotalAnimationTime_4();
		Assert_IsTrue_m2900904701(NULL /*static, unused*/, (bool)((((float)L_2) > ((float)(0.0f)))? 1 : 0), _stringLiteral120387999, /*hidden argument*/NULL);
		__this->set_timer_5((0.0f));
		return;
	}
}
// System.Void Tiled2Unity.TileAnimator::Update()
extern "C"  void TileAnimator_Update_m2637475796 (TileAnimator_t1251487515 * __this, const MethodInfo* method)
{
	MeshRenderer_t1268241104 * G_B5_0 = NULL;
	MeshRenderer_t1268241104 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	MeshRenderer_t1268241104 * G_B6_1 = NULL;
	{
		float L_0 = __this->get_timer_5();
		float L_1 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timer_5(((float)((float)L_0+(float)L_1)));
		goto IL_002d;
	}

IL_0018:
	{
		float L_2 = __this->get_timer_5();
		float L_3 = __this->get_TotalAnimationTime_4();
		__this->set_timer_5(((float)((float)L_2-(float)L_3)));
	}

IL_002d:
	{
		float L_4 = __this->get_timer_5();
		float L_5 = __this->get_TotalAnimationTime_4();
		if ((((float)L_4) > ((float)L_5)))
		{
			goto IL_0018;
		}
	}
	{
		MeshRenderer_t1268241104 * L_6 = __this->get_meshRenderer_6();
		float L_7 = __this->get_timer_5();
		float L_8 = __this->get_StartTime_2();
		G_B4_0 = L_6;
		if ((!(((float)L_7) >= ((float)L_8))))
		{
			G_B5_0 = L_6;
			goto IL_006c;
		}
	}
	{
		float L_9 = __this->get_timer_5();
		float L_10 = __this->get_StartTime_2();
		float L_11 = __this->get_Duration_3();
		G_B6_0 = ((((float)L_9) < ((float)((float)((float)L_10+(float)L_11))))? 1 : 0);
		G_B6_1 = G_B4_0;
		goto IL_006d;
	}

IL_006c:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_006d:
	{
		NullCheck(G_B6_1);
		Renderer_set_enabled_m142717579(G_B6_1, (bool)G_B6_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Tiled2Unity.TiledInitialShaderProperties::.ctor()
extern "C"  void TiledInitialShaderProperties__ctor_m169674058 (TiledInitialShaderProperties_t1898058332 * __this, const MethodInfo* method)
{
	{
		__this->set_InitialOpacity_2((1.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Tiled2Unity.TiledInitialShaderProperties::Awake()
extern const MethodInfo* GameObject_GetComponent_TisMeshRenderer_t1268241104_m3528968632_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral895546098;
extern const uint32_t TiledInitialShaderProperties_Awake_m2057389079_MetadataUsageId;
extern "C"  void TiledInitialShaderProperties_Awake_m2057389079 (TiledInitialShaderProperties_t1898058332 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TiledInitialShaderProperties_Awake_m2057389079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshRenderer_t1268241104 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		MeshRenderer_t1268241104 * L_1 = GameObject_GetComponent_TisMeshRenderer_t1268241104_m3528968632(L_0, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t1268241104_m3528968632_MethodInfo_var);
		V_0 = L_1;
		float L_2 = __this->get_InitialOpacity_2();
		if ((((float)L_2) == ((float)(1.0f))))
		{
			goto IL_005e;
		}
	}
	{
		MeshRenderer_t1268241104 * L_3 = V_0;
		NullCheck(L_3);
		Material_t193706927 * L_4 = Renderer_get_material_m2553789785(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = Material_HasProperty_m3511389613(L_4, _stringLiteral895546098, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005e;
		}
	}
	{
		MeshRenderer_t1268241104 * L_6 = V_0;
		NullCheck(L_6);
		Material_t193706927 * L_7 = Renderer_get_material_m2553789785(L_6, /*hidden argument*/NULL);
		float L_8 = __this->get_InitialOpacity_2();
		Color_t2020392075  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m1909920690(&L_9, (1.0f), (1.0f), (1.0f), L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetColor_m650857509(L_7, _stringLiteral895546098, L_9, /*hidden argument*/NULL);
	}

IL_005e:
	{
		return;
	}
}
// System.Void Tiled2Unity.TiledMap::.ctor()
extern "C"  void TiledMap__ctor_m2747019420 (TiledMap_t4203693682 * __this, const MethodInfo* method)
{
	{
		__this->set_Orientation_2(0);
		__this->set_StaggerAxis_3(0);
		__this->set_StaggerIndex_4(0);
		__this->set_HexSideLength_5(0);
		__this->set_NumLayers_6(0);
		__this->set_NumTilesWide_7(0);
		__this->set_NumTilesHigh_8(0);
		__this->set_TileWidth_9(0);
		__this->set_TileHeight_10(0);
		__this->set_ExportScale_11((1.0f));
		__this->set_MapWidthInPixels_12(0);
		__this->set_MapHeightInPixels_13(0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Tiled2Unity.TiledMap::GetMapWidthInPixelsScaled()
extern "C"  float TiledMap_GetMapWidthInPixelsScaled_m2351779916 (TiledMap_t4203693682 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		int32_t L_0 = __this->get_MapWidthInPixels_12();
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_lossyScale_m1638545862(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_1();
		float L_4 = __this->get_ExportScale_11();
		V_1 = ((float)((float)((float)((float)(((float)((float)L_0)))*(float)L_3))*(float)L_4));
		goto IL_0029;
	}

IL_0029:
	{
		float L_5 = V_1;
		return L_5;
	}
}
// System.Single Tiled2Unity.TiledMap::GetMapHeightInPixelsScaled()
extern "C"  float TiledMap_GetMapHeightInPixelsScaled_m737105483 (TiledMap_t4203693682 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		int32_t L_0 = __this->get_MapHeightInPixels_13();
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_lossyScale_m1638545862(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_y_2();
		float L_4 = __this->get_ExportScale_11();
		V_1 = ((float)((float)((float)((float)(((float)((float)L_0)))*(float)L_3))*(float)L_4));
		goto IL_0029;
	}

IL_0029:
	{
		float L_5 = V_1;
		return L_5;
	}
}
// UnityEngine.Rect Tiled2Unity.TiledMap::GetMapRect()
extern "C"  Rect_t3681755626  TiledMap_GetMapRect_m2769672278 (TiledMap_t4203693682 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Vector2_t2243707579  L_3 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = __this->get_MapWidthInPixels_12();
		V_1 = (((float)((float)L_4)));
		int32_t L_5 = __this->get_MapHeightInPixels_13();
		V_2 = (((float)((float)L_5)));
		float L_6 = (&V_0)->get_x_0();
		float L_7 = (&V_0)->get_y_1();
		float L_8 = V_2;
		float L_9 = V_1;
		float L_10 = V_2;
		Rect_t3681755626  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Rect__ctor_m1220545469(&L_11, L_6, ((float)((float)L_7-(float)L_8)), L_9, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		goto IL_0044;
	}

IL_0044:
	{
		Rect_t3681755626  L_12 = V_3;
		return L_12;
	}
}
// UnityEngine.Rect Tiled2Unity.TiledMap::GetMapRectInPixelsScaled()
extern "C"  Rect_t3681755626  TiledMap_GetMapRectInPixelsScaled_m2417645358 (TiledMap_t4203693682 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Vector2_t2243707579  L_3 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = TiledMap_GetMapWidthInPixelsScaled_m2351779916(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = TiledMap_GetMapHeightInPixelsScaled_m737105483(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = (&V_0)->get_x_0();
		float L_7 = (&V_0)->get_y_1();
		float L_8 = V_2;
		float L_9 = V_1;
		float L_10 = V_2;
		Rect_t3681755626  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Rect__ctor_m1220545469(&L_11, L_6, ((float)((float)L_7-(float)L_8)), L_9, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		goto IL_0042;
	}

IL_0042:
	{
		Rect_t3681755626  L_12 = V_3;
		return L_12;
	}
}
// System.Boolean Tiled2Unity.TiledMap::AreTilesStaggered()
extern "C"  bool TiledMap_AreTilesStaggered_m1869245225 (TiledMap_t4203693682 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = __this->get_Orientation_2();
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = __this->get_Orientation_2();
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)3))? 1 : 0);
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 1;
	}

IL_0019:
	{
		V_0 = (bool)G_B3_0;
		goto IL_001f;
	}

IL_001f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void Tiled2Unity.TiledMap::OnDrawGizmosSelected()
extern "C"  void TiledMap_OnDrawGizmosSelected_m1076805017 (TiledMap_t4203693682 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = V_0;
		Vector3_t2243707580  L_5 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = TiledMap_GetMapWidthInPixelsScaled_m2351779916(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m2720820983(&L_7, L_6, (0.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = V_0;
		Vector3_t2243707580  L_9 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = TiledMap_GetMapWidthInPixelsScaled_m2351779916(__this, /*hidden argument*/NULL);
		float L_11 = TiledMap_GetMapHeightInPixelsScaled_m737105483(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2720820983(&L_12, L_10, ((-L_11)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = V_0;
		Vector3_t2243707580  L_14 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		float L_15 = TiledMap_GetMapHeightInPixelsScaled_m737105483(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m2720820983(&L_16, (0.0f), ((-L_15)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_17 = V_0;
		Vector3_t2243707580  L_18 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		V_4 = L_18;
		int32_t L_19 = __this->get_NumLayers_6();
		V_5 = ((float)((float)(-1.0f)*(float)(((float)((float)L_19)))));
		Vector3_t2243707580 * L_20 = (&V_0);
		float L_21 = L_20->get_z_3();
		float L_22 = V_5;
		L_20->set_z_3(((float)((float)L_21+(float)L_22)));
		Vector3_t2243707580 * L_23 = (&V_1);
		float L_24 = L_23->get_z_3();
		float L_25 = V_5;
		L_23->set_z_3(((float)((float)L_24+(float)L_25)));
		Vector3_t2243707580 * L_26 = (&V_2);
		float L_27 = L_26->get_z_3();
		float L_28 = V_5;
		L_26->set_z_3(((float)((float)L_27+(float)L_28)));
		Vector3_t2243707580 * L_29 = (&V_3);
		float L_30 = L_29->get_z_3();
		float L_31 = V_5;
		L_29->set_z_3(((float)((float)L_30+(float)L_31)));
		Vector3_t2243707580 * L_32 = (&V_4);
		float L_33 = L_32->get_z_3();
		float L_34 = V_5;
		L_32->set_z_3(((float)((float)L_33+(float)L_34)));
		Color_t2020392075  L_35 = Color_get_blue_m4180825090(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		Vector3_t2243707580  L_36 = V_1;
		Vector3_t2243707580  L_37 = V_2;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		Vector3_t2243707580  L_38 = V_2;
		Vector3_t2243707580  L_39 = V_3;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
		Vector3_t2243707580  L_40 = V_3;
		Vector3_t2243707580  L_41 = V_4;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		Vector3_t2243707580  L_42 = V_4;
		Vector3_t2243707580  L_43 = V_1;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_42, L_43, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Tiled2Unity.TileObject::.ctor()
extern "C"  void TileObject__ctor_m4180489253 (TileObject_t2436995085 * __this, const MethodInfo* method)
{
	{
		__this->set_TileWidth_2((0.0f));
		__this->set_TileHeight_3((0.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Timer::.ctor()
extern "C"  void Timer__ctor_m128890648 (Timer_t2917042437 * __this, const MethodInfo* method)
{
	{
		__this->set_timeLeft_2((200.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Timer::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral474265194;
extern Il2CppCodeGenString* _stringLiteral1884423134;
extern const uint32_t Timer_Update_m1869297125_MetadataUsageId;
extern "C"  void Timer_Update_m1869297125 (Timer_t2917042437 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Timer_Update_m1869297125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_timeLeft_2();
		float L_1 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timeLeft_2(((float)((float)L_0-(float)L_1)));
		Text_t356221433 * L_2 = __this->get_timerText_3();
		float L_3 = __this->get_timeLeft_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = bankers_roundf(L_3);
		float L_5 = L_4;
		Il2CppObject * L_6 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral474265194, L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_7);
		float L_8 = __this->get_timeLeft_2();
		if ((!(((float)L_8) < ((float)(0.0f)))))
		{
			goto IL_0054;
		}
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral1884423134, /*hidden argument*/NULL);
	}

IL_0054:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
