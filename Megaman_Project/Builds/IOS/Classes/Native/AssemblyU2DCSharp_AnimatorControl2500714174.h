﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AnimatorControl
struct AnimatorControl_t2500714174;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimatorControl
struct  AnimatorControl_t2500714174  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform AnimatorControl::myTransformAnim
	Transform_t3275118058 * ___myTransformAnim_3;
	// UnityEngine.Animator AnimatorControl::myAnimAnimator
	Animator_t69676727 * ___myAnimAnimator_4;
	// UnityEngine.Vector3 AnimatorControl::artScale
	Vector3_t2243707580  ___artScale_5;

public:
	inline static int32_t get_offset_of_myTransformAnim_3() { return static_cast<int32_t>(offsetof(AnimatorControl_t2500714174, ___myTransformAnim_3)); }
	inline Transform_t3275118058 * get_myTransformAnim_3() const { return ___myTransformAnim_3; }
	inline Transform_t3275118058 ** get_address_of_myTransformAnim_3() { return &___myTransformAnim_3; }
	inline void set_myTransformAnim_3(Transform_t3275118058 * value)
	{
		___myTransformAnim_3 = value;
		Il2CppCodeGenWriteBarrier(&___myTransformAnim_3, value);
	}

	inline static int32_t get_offset_of_myAnimAnimator_4() { return static_cast<int32_t>(offsetof(AnimatorControl_t2500714174, ___myAnimAnimator_4)); }
	inline Animator_t69676727 * get_myAnimAnimator_4() const { return ___myAnimAnimator_4; }
	inline Animator_t69676727 ** get_address_of_myAnimAnimator_4() { return &___myAnimAnimator_4; }
	inline void set_myAnimAnimator_4(Animator_t69676727 * value)
	{
		___myAnimAnimator_4 = value;
		Il2CppCodeGenWriteBarrier(&___myAnimAnimator_4, value);
	}

	inline static int32_t get_offset_of_artScale_5() { return static_cast<int32_t>(offsetof(AnimatorControl_t2500714174, ___artScale_5)); }
	inline Vector3_t2243707580  get_artScale_5() const { return ___artScale_5; }
	inline Vector3_t2243707580 * get_address_of_artScale_5() { return &___artScale_5; }
	inline void set_artScale_5(Vector3_t2243707580  value)
	{
		___artScale_5 = value;
	}
};

struct AnimatorControl_t2500714174_StaticFields
{
public:
	// AnimatorControl AnimatorControl::instance
	AnimatorControl_t2500714174 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(AnimatorControl_t2500714174_StaticFields, ___instance_2)); }
	inline AnimatorControl_t2500714174 * get_instance_2() const { return ___instance_2; }
	inline AnimatorControl_t2500714174 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(AnimatorControl_t2500714174 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
