﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayerController
struct PlayerController_t4148409433;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shooting
struct  Shooting_t3467275689  : public MonoBehaviour_t1158329972
{
public:
	// PlayerController Shooting::player
	PlayerController_t4148409433 * ___player_2;
	// UnityEngine.Rigidbody2D Shooting::rb
	Rigidbody2D_t502193897 * ___rb_3;
	// System.Single Shooting::shootSpeed
	float ___shootSpeed_4;
	// System.Int32 Shooting::bulletDamage
	int32_t ___bulletDamage_5;

public:
	inline static int32_t get_offset_of_player_2() { return static_cast<int32_t>(offsetof(Shooting_t3467275689, ___player_2)); }
	inline PlayerController_t4148409433 * get_player_2() const { return ___player_2; }
	inline PlayerController_t4148409433 ** get_address_of_player_2() { return &___player_2; }
	inline void set_player_2(PlayerController_t4148409433 * value)
	{
		___player_2 = value;
		Il2CppCodeGenWriteBarrier(&___player_2, value);
	}

	inline static int32_t get_offset_of_rb_3() { return static_cast<int32_t>(offsetof(Shooting_t3467275689, ___rb_3)); }
	inline Rigidbody2D_t502193897 * get_rb_3() const { return ___rb_3; }
	inline Rigidbody2D_t502193897 ** get_address_of_rb_3() { return &___rb_3; }
	inline void set_rb_3(Rigidbody2D_t502193897 * value)
	{
		___rb_3 = value;
		Il2CppCodeGenWriteBarrier(&___rb_3, value);
	}

	inline static int32_t get_offset_of_shootSpeed_4() { return static_cast<int32_t>(offsetof(Shooting_t3467275689, ___shootSpeed_4)); }
	inline float get_shootSpeed_4() const { return ___shootSpeed_4; }
	inline float* get_address_of_shootSpeed_4() { return &___shootSpeed_4; }
	inline void set_shootSpeed_4(float value)
	{
		___shootSpeed_4 = value;
	}

	inline static int32_t get_offset_of_bulletDamage_5() { return static_cast<int32_t>(offsetof(Shooting_t3467275689, ___bulletDamage_5)); }
	inline int32_t get_bulletDamage_5() const { return ___bulletDamage_5; }
	inline int32_t* get_address_of_bulletDamage_5() { return &___bulletDamage_5; }
	inline void set_bulletDamage_5(int32_t value)
	{
		___bulletDamage_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
