﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GreenSpinner
struct  GreenSpinner_t3572071360  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Rigidbody2D GreenSpinner::rb
	Rigidbody2D_t502193897 * ___rb_2;
	// UnityEngine.Transform GreenSpinner::enemyTrans
	Transform_t3275118058 * ___enemyTrans_3;
	// UnityEngine.Transform GreenSpinner::firePoint
	Transform_t3275118058 * ___firePoint_4;
	// UnityEngine.GameObject GreenSpinner::bullet
	GameObject_t1756533147 * ___bullet_5;
	// System.Single GreenSpinner::moveSpeed
	float ___moveSpeed_6;
	// System.Single GreenSpinner::seconds
	float ___seconds_7;
	// System.Int32 GreenSpinner::directionVal
	int32_t ___directionVal_8;

public:
	inline static int32_t get_offset_of_rb_2() { return static_cast<int32_t>(offsetof(GreenSpinner_t3572071360, ___rb_2)); }
	inline Rigidbody2D_t502193897 * get_rb_2() const { return ___rb_2; }
	inline Rigidbody2D_t502193897 ** get_address_of_rb_2() { return &___rb_2; }
	inline void set_rb_2(Rigidbody2D_t502193897 * value)
	{
		___rb_2 = value;
		Il2CppCodeGenWriteBarrier(&___rb_2, value);
	}

	inline static int32_t get_offset_of_enemyTrans_3() { return static_cast<int32_t>(offsetof(GreenSpinner_t3572071360, ___enemyTrans_3)); }
	inline Transform_t3275118058 * get_enemyTrans_3() const { return ___enemyTrans_3; }
	inline Transform_t3275118058 ** get_address_of_enemyTrans_3() { return &___enemyTrans_3; }
	inline void set_enemyTrans_3(Transform_t3275118058 * value)
	{
		___enemyTrans_3 = value;
		Il2CppCodeGenWriteBarrier(&___enemyTrans_3, value);
	}

	inline static int32_t get_offset_of_firePoint_4() { return static_cast<int32_t>(offsetof(GreenSpinner_t3572071360, ___firePoint_4)); }
	inline Transform_t3275118058 * get_firePoint_4() const { return ___firePoint_4; }
	inline Transform_t3275118058 ** get_address_of_firePoint_4() { return &___firePoint_4; }
	inline void set_firePoint_4(Transform_t3275118058 * value)
	{
		___firePoint_4 = value;
		Il2CppCodeGenWriteBarrier(&___firePoint_4, value);
	}

	inline static int32_t get_offset_of_bullet_5() { return static_cast<int32_t>(offsetof(GreenSpinner_t3572071360, ___bullet_5)); }
	inline GameObject_t1756533147 * get_bullet_5() const { return ___bullet_5; }
	inline GameObject_t1756533147 ** get_address_of_bullet_5() { return &___bullet_5; }
	inline void set_bullet_5(GameObject_t1756533147 * value)
	{
		___bullet_5 = value;
		Il2CppCodeGenWriteBarrier(&___bullet_5, value);
	}

	inline static int32_t get_offset_of_moveSpeed_6() { return static_cast<int32_t>(offsetof(GreenSpinner_t3572071360, ___moveSpeed_6)); }
	inline float get_moveSpeed_6() const { return ___moveSpeed_6; }
	inline float* get_address_of_moveSpeed_6() { return &___moveSpeed_6; }
	inline void set_moveSpeed_6(float value)
	{
		___moveSpeed_6 = value;
	}

	inline static int32_t get_offset_of_seconds_7() { return static_cast<int32_t>(offsetof(GreenSpinner_t3572071360, ___seconds_7)); }
	inline float get_seconds_7() const { return ___seconds_7; }
	inline float* get_address_of_seconds_7() { return &___seconds_7; }
	inline void set_seconds_7(float value)
	{
		___seconds_7 = value;
	}

	inline static int32_t get_offset_of_directionVal_8() { return static_cast<int32_t>(offsetof(GreenSpinner_t3572071360, ___directionVal_8)); }
	inline int32_t get_directionVal_8() const { return ___directionVal_8; }
	inline int32_t* get_address_of_directionVal_8() { return &___directionVal_8; }
	inline void set_directionVal_8(int32_t value)
	{
		___directionVal_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
