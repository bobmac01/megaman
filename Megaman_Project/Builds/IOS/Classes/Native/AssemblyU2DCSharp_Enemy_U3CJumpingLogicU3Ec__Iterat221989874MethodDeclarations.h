﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Enemy/<JumpingLogic>c__Iterator0
struct U3CJumpingLogicU3Ec__Iterator0_t221989874;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Enemy/<JumpingLogic>c__Iterator0::.ctor()
extern "C"  void U3CJumpingLogicU3Ec__Iterator0__ctor_m3781887137 (U3CJumpingLogicU3Ec__Iterator0_t221989874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Enemy/<JumpingLogic>c__Iterator0::MoveNext()
extern "C"  bool U3CJumpingLogicU3Ec__Iterator0_MoveNext_m2179458059 (U3CJumpingLogicU3Ec__Iterator0_t221989874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Enemy/<JumpingLogic>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CJumpingLogicU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4017147243 (U3CJumpingLogicU3Ec__Iterator0_t221989874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Enemy/<JumpingLogic>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CJumpingLogicU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3360114851 (U3CJumpingLogicU3Ec__Iterator0_t221989874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy/<JumpingLogic>c__Iterator0::Dispose()
extern "C"  void U3CJumpingLogicU3Ec__Iterator0_Dispose_m50255436 (U3CJumpingLogicU3Ec__Iterator0_t221989874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy/<JumpingLogic>c__Iterator0::Reset()
extern "C"  void U3CJumpingLogicU3Ec__Iterator0_Reset_m322117890 (U3CJumpingLogicU3Ec__Iterator0_t221989874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
