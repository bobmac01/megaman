﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ladder
struct Ladder_t3942361356;

#include "codegen/il2cpp-codegen.h"

// System.Void Ladder::.ctor()
extern "C"  void Ladder__ctor_m1239273833 (Ladder_t3942361356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ladder::Start()
extern "C"  void Ladder_Start_m2760985817 (Ladder_t3942361356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ladder::UpdateClimb(System.Boolean)
extern "C"  void Ladder_UpdateClimb_m3328543748 (Ladder_t3942361356 * __this, bool ___canClimb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
