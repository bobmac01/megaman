﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tiled2Unity.TileObject
struct  TileObject_t2436995085  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Tiled2Unity.TileObject::TileWidth
	float ___TileWidth_2;
	// System.Single Tiled2Unity.TileObject::TileHeight
	float ___TileHeight_3;

public:
	inline static int32_t get_offset_of_TileWidth_2() { return static_cast<int32_t>(offsetof(TileObject_t2436995085, ___TileWidth_2)); }
	inline float get_TileWidth_2() const { return ___TileWidth_2; }
	inline float* get_address_of_TileWidth_2() { return &___TileWidth_2; }
	inline void set_TileWidth_2(float value)
	{
		___TileWidth_2 = value;
	}

	inline static int32_t get_offset_of_TileHeight_3() { return static_cast<int32_t>(offsetof(TileObject_t2436995085, ___TileHeight_3)); }
	inline float get_TileHeight_3() const { return ___TileHeight_3; }
	inline float* get_address_of_TileHeight_3() { return &___TileHeight_3; }
	inline void set_TileHeight_3(float value)
	{
		___TileHeight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
