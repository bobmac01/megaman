﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerHealth
struct  PlayerHealth_t2894595013  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 PlayerHealth::maxHealth
	int32_t ___maxHealth_2;

public:
	inline static int32_t get_offset_of_maxHealth_2() { return static_cast<int32_t>(offsetof(PlayerHealth_t2894595013, ___maxHealth_2)); }
	inline int32_t get_maxHealth_2() const { return ___maxHealth_2; }
	inline int32_t* get_address_of_maxHealth_2() { return &___maxHealth_2; }
	inline void set_maxHealth_2(int32_t value)
	{
		___maxHealth_2 = value;
	}
};

struct PlayerHealth_t2894595013_StaticFields
{
public:
	// System.Int32 PlayerHealth::currentHealth
	int32_t ___currentHealth_3;

public:
	inline static int32_t get_offset_of_currentHealth_3() { return static_cast<int32_t>(offsetof(PlayerHealth_t2894595013_StaticFields, ___currentHealth_3)); }
	inline int32_t get_currentHealth_3() const { return ___currentHealth_3; }
	inline int32_t* get_address_of_currentHealth_3() { return &___currentHealth_3; }
	inline void set_currentHealth_3(int32_t value)
	{
		___currentHealth_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
