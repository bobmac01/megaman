﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GreenSpinner/<WaitShoot>c__Iterator0
struct U3CWaitShootU3Ec__Iterator0_t2318521998;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GreenSpinner/<WaitShoot>c__Iterator0::.ctor()
extern "C"  void U3CWaitShootU3Ec__Iterator0__ctor_m306744893 (U3CWaitShootU3Ec__Iterator0_t2318521998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GreenSpinner/<WaitShoot>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitShootU3Ec__Iterator0_MoveNext_m261123167 (U3CWaitShootU3Ec__Iterator0_t2318521998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GreenSpinner/<WaitShoot>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitShootU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3685701799 (U3CWaitShootU3Ec__Iterator0_t2318521998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GreenSpinner/<WaitShoot>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitShootU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3559192559 (U3CWaitShootU3Ec__Iterator0_t2318521998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreenSpinner/<WaitShoot>c__Iterator0::Dispose()
extern "C"  void U3CWaitShootU3Ec__Iterator0_Dispose_m394302096 (U3CWaitShootU3Ec__Iterator0_t2318521998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreenSpinner/<WaitShoot>c__Iterator0::Reset()
extern "C"  void U3CWaitShootU3Ec__Iterator0_Reset_m2880820038 (U3CWaitShootU3Ec__Iterator0_t2318521998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
