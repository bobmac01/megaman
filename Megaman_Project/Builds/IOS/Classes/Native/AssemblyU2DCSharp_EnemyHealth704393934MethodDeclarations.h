﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnemyHealth
struct EnemyHealth_t704393934;

#include "codegen/il2cpp-codegen.h"

// System.Void EnemyHealth::.ctor()
extern "C"  void EnemyHealth__ctor_m2679466021 (EnemyHealth_t704393934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyHealth::Start()
extern "C"  void EnemyHealth_Start_m2934489173 (EnemyHealth_t704393934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyHealth::TakeDamage(System.Int32)
extern "C"  void EnemyHealth_TakeDamage_m3053861170 (EnemyHealth_t704393934 * __this, int32_t ___amount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyHealth::ProcessEnemyDeath()
extern "C"  void EnemyHealth_ProcessEnemyDeath_m2535131214 (EnemyHealth_t704393934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
