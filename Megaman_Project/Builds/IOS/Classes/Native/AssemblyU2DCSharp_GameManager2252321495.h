﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t2252321495  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject GameManager::playerObject
	GameObject_t1756533147 * ___playerObject_2;
	// System.Int32 GameManager::points
	int32_t ___points_5;

public:
	inline static int32_t get_offset_of_playerObject_2() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___playerObject_2)); }
	inline GameObject_t1756533147 * get_playerObject_2() const { return ___playerObject_2; }
	inline GameObject_t1756533147 ** get_address_of_playerObject_2() { return &___playerObject_2; }
	inline void set_playerObject_2(GameObject_t1756533147 * value)
	{
		___playerObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___playerObject_2, value);
	}

	inline static int32_t get_offset_of_points_5() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___points_5)); }
	inline int32_t get_points_5() const { return ___points_5; }
	inline int32_t* get_address_of_points_5() { return &___points_5; }
	inline void set_points_5(int32_t value)
	{
		___points_5 = value;
	}
};

struct GameManager_t2252321495_StaticFields
{
public:
	// System.Boolean GameManager::isDead
	bool ___isDead_3;
	// System.Boolean GameManager::levelCompleted
	bool ___levelCompleted_4;

public:
	inline static int32_t get_offset_of_isDead_3() { return static_cast<int32_t>(offsetof(GameManager_t2252321495_StaticFields, ___isDead_3)); }
	inline bool get_isDead_3() const { return ___isDead_3; }
	inline bool* get_address_of_isDead_3() { return &___isDead_3; }
	inline void set_isDead_3(bool value)
	{
		___isDead_3 = value;
	}

	inline static int32_t get_offset_of_levelCompleted_4() { return static_cast<int32_t>(offsetof(GameManager_t2252321495_StaticFields, ___levelCompleted_4)); }
	inline bool get_levelCompleted_4() const { return ___levelCompleted_4; }
	inline bool* get_address_of_levelCompleted_4() { return &___levelCompleted_4; }
	inline void set_levelCompleted_4(bool value)
	{
		___levelCompleted_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
