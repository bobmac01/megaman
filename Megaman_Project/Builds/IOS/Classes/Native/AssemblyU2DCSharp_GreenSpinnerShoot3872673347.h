﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// GreenSpinner
struct GreenSpinner_t3572071360;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GreenSpinnerShoot
struct  GreenSpinnerShoot_t3872673347  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Rigidbody2D GreenSpinnerShoot::rb
	Rigidbody2D_t502193897 * ___rb_2;
	// GreenSpinner GreenSpinnerShoot::spinner
	GreenSpinner_t3572071360 * ___spinner_3;
	// System.Single GreenSpinnerShoot::shootSpeed
	float ___shootSpeed_4;
	// System.Int32 GreenSpinnerShoot::bulletDamage
	int32_t ___bulletDamage_5;

public:
	inline static int32_t get_offset_of_rb_2() { return static_cast<int32_t>(offsetof(GreenSpinnerShoot_t3872673347, ___rb_2)); }
	inline Rigidbody2D_t502193897 * get_rb_2() const { return ___rb_2; }
	inline Rigidbody2D_t502193897 ** get_address_of_rb_2() { return &___rb_2; }
	inline void set_rb_2(Rigidbody2D_t502193897 * value)
	{
		___rb_2 = value;
		Il2CppCodeGenWriteBarrier(&___rb_2, value);
	}

	inline static int32_t get_offset_of_spinner_3() { return static_cast<int32_t>(offsetof(GreenSpinnerShoot_t3872673347, ___spinner_3)); }
	inline GreenSpinner_t3572071360 * get_spinner_3() const { return ___spinner_3; }
	inline GreenSpinner_t3572071360 ** get_address_of_spinner_3() { return &___spinner_3; }
	inline void set_spinner_3(GreenSpinner_t3572071360 * value)
	{
		___spinner_3 = value;
		Il2CppCodeGenWriteBarrier(&___spinner_3, value);
	}

	inline static int32_t get_offset_of_shootSpeed_4() { return static_cast<int32_t>(offsetof(GreenSpinnerShoot_t3872673347, ___shootSpeed_4)); }
	inline float get_shootSpeed_4() const { return ___shootSpeed_4; }
	inline float* get_address_of_shootSpeed_4() { return &___shootSpeed_4; }
	inline void set_shootSpeed_4(float value)
	{
		___shootSpeed_4 = value;
	}

	inline static int32_t get_offset_of_bulletDamage_5() { return static_cast<int32_t>(offsetof(GreenSpinnerShoot_t3872673347, ___bulletDamage_5)); }
	inline int32_t get_bulletDamage_5() const { return ___bulletDamage_5; }
	inline int32_t* get_address_of_bulletDamage_5() { return &___bulletDamage_5; }
	inline void set_bulletDamage_5(int32_t value)
	{
		___bulletDamage_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
