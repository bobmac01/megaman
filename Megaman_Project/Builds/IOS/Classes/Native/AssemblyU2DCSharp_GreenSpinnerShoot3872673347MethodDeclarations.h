﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GreenSpinnerShoot
struct GreenSpinnerShoot_t3872673347;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"

// System.Void GreenSpinnerShoot::.ctor()
extern "C"  void GreenSpinnerShoot__ctor_m2941445414 (GreenSpinnerShoot_t3872673347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreenSpinnerShoot::Start()
extern "C"  void GreenSpinnerShoot_Start_m293141642 (GreenSpinnerShoot_t3872673347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreenSpinnerShoot::Update()
extern "C"  void GreenSpinnerShoot_Update_m4212651139 (GreenSpinnerShoot_t3872673347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreenSpinnerShoot::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void GreenSpinnerShoot_OnCollisionEnter2D_m2476808972 (GreenSpinnerShoot_t3872673347 * __this, Collision2D_t1539500754 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
