﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Assertions.AssertionException
struct AssertionException_t3713171805;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Assertions.AssertionException::.ctor(System.String,System.String)
extern "C"  void AssertionException__ctor_m456374519 (AssertionException_t3713171805 * __this, String_t* ___message0, String_t* ___userMessage1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Assertions.AssertionException::get_Message()
extern "C"  String_t* AssertionException_get_Message_m1380549180 (AssertionException_t3713171805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
