﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Shooting
struct Shooting_t3467275689;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"

// System.Void Shooting::.ctor()
extern "C"  void Shooting__ctor_m2748436320 (Shooting_t3467275689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shooting::Start()
extern "C"  void Shooting_Start_m1677191848 (Shooting_t3467275689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shooting::Update()
extern "C"  void Shooting_Update_m1217253073 (Shooting_t3467275689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shooting::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void Shooting_OnCollisionEnter2D_m3649009394 (Shooting_t3467275689 * __this, Collision2D_t1539500754 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
