﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MovingPlatform
struct  MovingPlatform_t2706464437  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Rigidbody2D MovingPlatform::rb
	Rigidbody2D_t502193897 * ___rb_2;
	// UnityEngine.Transform MovingPlatform::platformTrans
	Transform_t3275118058 * ___platformTrans_3;
	// UnityEngine.Animator MovingPlatform::myAnim
	Animator_t69676727 * ___myAnim_4;
	// System.Single MovingPlatform::moveSpeed
	float ___moveSpeed_5;
	// System.Int32 MovingPlatform::directionVal
	int32_t ___directionVal_6;

public:
	inline static int32_t get_offset_of_rb_2() { return static_cast<int32_t>(offsetof(MovingPlatform_t2706464437, ___rb_2)); }
	inline Rigidbody2D_t502193897 * get_rb_2() const { return ___rb_2; }
	inline Rigidbody2D_t502193897 ** get_address_of_rb_2() { return &___rb_2; }
	inline void set_rb_2(Rigidbody2D_t502193897 * value)
	{
		___rb_2 = value;
		Il2CppCodeGenWriteBarrier(&___rb_2, value);
	}

	inline static int32_t get_offset_of_platformTrans_3() { return static_cast<int32_t>(offsetof(MovingPlatform_t2706464437, ___platformTrans_3)); }
	inline Transform_t3275118058 * get_platformTrans_3() const { return ___platformTrans_3; }
	inline Transform_t3275118058 ** get_address_of_platformTrans_3() { return &___platformTrans_3; }
	inline void set_platformTrans_3(Transform_t3275118058 * value)
	{
		___platformTrans_3 = value;
		Il2CppCodeGenWriteBarrier(&___platformTrans_3, value);
	}

	inline static int32_t get_offset_of_myAnim_4() { return static_cast<int32_t>(offsetof(MovingPlatform_t2706464437, ___myAnim_4)); }
	inline Animator_t69676727 * get_myAnim_4() const { return ___myAnim_4; }
	inline Animator_t69676727 ** get_address_of_myAnim_4() { return &___myAnim_4; }
	inline void set_myAnim_4(Animator_t69676727 * value)
	{
		___myAnim_4 = value;
		Il2CppCodeGenWriteBarrier(&___myAnim_4, value);
	}

	inline static int32_t get_offset_of_moveSpeed_5() { return static_cast<int32_t>(offsetof(MovingPlatform_t2706464437, ___moveSpeed_5)); }
	inline float get_moveSpeed_5() const { return ___moveSpeed_5; }
	inline float* get_address_of_moveSpeed_5() { return &___moveSpeed_5; }
	inline void set_moveSpeed_5(float value)
	{
		___moveSpeed_5 = value;
	}

	inline static int32_t get_offset_of_directionVal_6() { return static_cast<int32_t>(offsetof(MovingPlatform_t2706464437, ___directionVal_6)); }
	inline int32_t get_directionVal_6() const { return ___directionVal_6; }
	inline int32_t* get_address_of_directionVal_6() { return &___directionVal_6; }
	inline void set_directionVal_6(int32_t value)
	{
		___directionVal_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
