﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimatorControl
struct AnimatorControl_t2500714174;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimatorControl::.ctor()
extern "C"  void AnimatorControl__ctor_m637738423 (AnimatorControl_t2500714174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatorControl::Start()
extern "C"  void AnimatorControl_Start_m1399485763 (AnimatorControl_t2500714174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatorControl::FlipPlayer(System.Single)
extern "C"  void AnimatorControl_FlipPlayer_m892024196 (AnimatorControl_t2500714174 * __this, float ___currentspeed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatorControl::UpdateSpeed(System.Single)
extern "C"  void AnimatorControl_UpdateSpeed_m2943783130 (AnimatorControl_t2500714174 * __this, float ___currentspeed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatorControl::UpdateGrounded(System.Boolean)
extern "C"  void AnimatorControl_UpdateGrounded_m956117693 (AnimatorControl_t2500714174 * __this, bool ___ground0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatorControl::UpdateClimbing(System.Boolean)
extern "C"  void AnimatorControl_UpdateClimbing_m1213998286 (AnimatorControl_t2500714174 * __this, bool ___climb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatorControl::UpdateShoot(System.Boolean)
extern "C"  void AnimatorControl_UpdateShoot_m2394759276 (AnimatorControl_t2500714174 * __this, bool ___shoot0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
