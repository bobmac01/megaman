﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerHealth
struct PlayerHealth_t2894595013;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerHealth::.ctor()
extern "C"  void PlayerHealth__ctor_m24469338 (PlayerHealth_t2894595013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerHealth::Update()
extern "C"  void PlayerHealth_Update_m682785001 (PlayerHealth_t2894595013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerHealth::TakeDamage(System.Int32)
extern "C"  void PlayerHealth_TakeDamage_m3822532457 (PlayerHealth_t2894595013 * __this, int32_t ___amount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerHealth::HealthPickup(System.Int32)
extern "C"  void PlayerHealth_HealthPickup_m528866477 (PlayerHealth_t2894595013 * __this, int32_t ___toGive0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerHealth::PlayerDrown()
extern "C"  void PlayerHealth_PlayerDrown_m3178492883 (PlayerHealth_t2894595013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerHealth::.cctor()
extern "C"  void PlayerHealth__cctor_m3084359511 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
