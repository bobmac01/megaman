﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Enemy
struct Enemy_t1088811588;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"

// System.Void Enemy::.ctor()
extern "C"  void Enemy__ctor_m2474411757 (Enemy_t1088811588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy::Start()
extern "C"  void Enemy_Start_m4271536181 (Enemy_t1088811588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy::Update()
extern "C"  void Enemy_Update_m1726793138 (Enemy_t1088811588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void Enemy_OnTriggerEnter2D_m990861181 (Enemy_t1088811588 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void Enemy_OnTriggerStay2D_m1909694772 (Enemy_t1088811588 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy::MoveEnemy()
extern "C"  void Enemy_MoveEnemy_m175095160 (Enemy_t1088811588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Enemy::JumpingLogic()
extern "C"  Il2CppObject * Enemy_JumpingLogic_m15362995 (Enemy_t1088811588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
