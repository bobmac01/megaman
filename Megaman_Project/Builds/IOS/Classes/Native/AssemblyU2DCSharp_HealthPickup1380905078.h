﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayerHealth
struct PlayerHealth_t2894595013;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HealthPickup
struct  HealthPickup_t1380905078  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 HealthPickup::healthToGive
	int32_t ___healthToGive_2;
	// PlayerHealth HealthPickup::pHealth
	PlayerHealth_t2894595013 * ___pHealth_3;

public:
	inline static int32_t get_offset_of_healthToGive_2() { return static_cast<int32_t>(offsetof(HealthPickup_t1380905078, ___healthToGive_2)); }
	inline int32_t get_healthToGive_2() const { return ___healthToGive_2; }
	inline int32_t* get_address_of_healthToGive_2() { return &___healthToGive_2; }
	inline void set_healthToGive_2(int32_t value)
	{
		___healthToGive_2 = value;
	}

	inline static int32_t get_offset_of_pHealth_3() { return static_cast<int32_t>(offsetof(HealthPickup_t1380905078, ___pHealth_3)); }
	inline PlayerHealth_t2894595013 * get_pHealth_3() const { return ___pHealth_3; }
	inline PlayerHealth_t2894595013 ** get_address_of_pHealth_3() { return &___pHealth_3; }
	inline void set_pHealth_3(PlayerHealth_t2894595013 * value)
	{
		___pHealth_3 = value;
		Il2CppCodeGenWriteBarrier(&___pHealth_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
