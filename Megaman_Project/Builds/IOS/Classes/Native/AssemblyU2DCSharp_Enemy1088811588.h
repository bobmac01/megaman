﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enemy
struct  Enemy_t1088811588  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Rigidbody2D Enemy::rb
	Rigidbody2D_t502193897 * ___rb_2;
	// UnityEngine.Transform Enemy::enemyTrans
	Transform_t3275118058 * ___enemyTrans_3;
	// UnityEngine.Animator Enemy::myAnim
	Animator_t69676727 * ___myAnim_4;
	// System.Single Enemy::moveSpeed
	float ___moveSpeed_5;
	// System.Single Enemy::jumpSpeed
	float ___jumpSpeed_6;
	// System.Single Enemy::health
	float ___health_7;
	// System.Single Enemy::seconds
	float ___seconds_8;
	// System.Int32 Enemy::enemydefect
	int32_t ___enemydefect_9;
	// System.Boolean Enemy::moveRight
	bool ___moveRight_10;
	// UnityEngine.LayerMask Enemy::enemyMask
	LayerMask_t3188175821  ___enemyMask_11;
	// System.Boolean Enemy::isGrounded
	bool ___isGrounded_12;
	// UnityEngine.Transform Enemy::groundTag
	Transform_t3275118058 * ___groundTag_13;

public:
	inline static int32_t get_offset_of_rb_2() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___rb_2)); }
	inline Rigidbody2D_t502193897 * get_rb_2() const { return ___rb_2; }
	inline Rigidbody2D_t502193897 ** get_address_of_rb_2() { return &___rb_2; }
	inline void set_rb_2(Rigidbody2D_t502193897 * value)
	{
		___rb_2 = value;
		Il2CppCodeGenWriteBarrier(&___rb_2, value);
	}

	inline static int32_t get_offset_of_enemyTrans_3() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___enemyTrans_3)); }
	inline Transform_t3275118058 * get_enemyTrans_3() const { return ___enemyTrans_3; }
	inline Transform_t3275118058 ** get_address_of_enemyTrans_3() { return &___enemyTrans_3; }
	inline void set_enemyTrans_3(Transform_t3275118058 * value)
	{
		___enemyTrans_3 = value;
		Il2CppCodeGenWriteBarrier(&___enemyTrans_3, value);
	}

	inline static int32_t get_offset_of_myAnim_4() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___myAnim_4)); }
	inline Animator_t69676727 * get_myAnim_4() const { return ___myAnim_4; }
	inline Animator_t69676727 ** get_address_of_myAnim_4() { return &___myAnim_4; }
	inline void set_myAnim_4(Animator_t69676727 * value)
	{
		___myAnim_4 = value;
		Il2CppCodeGenWriteBarrier(&___myAnim_4, value);
	}

	inline static int32_t get_offset_of_moveSpeed_5() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___moveSpeed_5)); }
	inline float get_moveSpeed_5() const { return ___moveSpeed_5; }
	inline float* get_address_of_moveSpeed_5() { return &___moveSpeed_5; }
	inline void set_moveSpeed_5(float value)
	{
		___moveSpeed_5 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_6() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___jumpSpeed_6)); }
	inline float get_jumpSpeed_6() const { return ___jumpSpeed_6; }
	inline float* get_address_of_jumpSpeed_6() { return &___jumpSpeed_6; }
	inline void set_jumpSpeed_6(float value)
	{
		___jumpSpeed_6 = value;
	}

	inline static int32_t get_offset_of_health_7() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___health_7)); }
	inline float get_health_7() const { return ___health_7; }
	inline float* get_address_of_health_7() { return &___health_7; }
	inline void set_health_7(float value)
	{
		___health_7 = value;
	}

	inline static int32_t get_offset_of_seconds_8() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___seconds_8)); }
	inline float get_seconds_8() const { return ___seconds_8; }
	inline float* get_address_of_seconds_8() { return &___seconds_8; }
	inline void set_seconds_8(float value)
	{
		___seconds_8 = value;
	}

	inline static int32_t get_offset_of_enemydefect_9() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___enemydefect_9)); }
	inline int32_t get_enemydefect_9() const { return ___enemydefect_9; }
	inline int32_t* get_address_of_enemydefect_9() { return &___enemydefect_9; }
	inline void set_enemydefect_9(int32_t value)
	{
		___enemydefect_9 = value;
	}

	inline static int32_t get_offset_of_moveRight_10() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___moveRight_10)); }
	inline bool get_moveRight_10() const { return ___moveRight_10; }
	inline bool* get_address_of_moveRight_10() { return &___moveRight_10; }
	inline void set_moveRight_10(bool value)
	{
		___moveRight_10 = value;
	}

	inline static int32_t get_offset_of_enemyMask_11() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___enemyMask_11)); }
	inline LayerMask_t3188175821  get_enemyMask_11() const { return ___enemyMask_11; }
	inline LayerMask_t3188175821 * get_address_of_enemyMask_11() { return &___enemyMask_11; }
	inline void set_enemyMask_11(LayerMask_t3188175821  value)
	{
		___enemyMask_11 = value;
	}

	inline static int32_t get_offset_of_isGrounded_12() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___isGrounded_12)); }
	inline bool get_isGrounded_12() const { return ___isGrounded_12; }
	inline bool* get_address_of_isGrounded_12() { return &___isGrounded_12; }
	inline void set_isGrounded_12(bool value)
	{
		___isGrounded_12 = value;
	}

	inline static int32_t get_offset_of_groundTag_13() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___groundTag_13)); }
	inline Transform_t3275118058 * get_groundTag_13() const { return ___groundTag_13; }
	inline Transform_t3275118058 ** get_address_of_groundTag_13() { return &___groundTag_13; }
	inline void set_groundTag_13(Transform_t3275118058 * value)
	{
		___groundTag_13 = value;
		Il2CppCodeGenWriteBarrier(&___groundTag_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
