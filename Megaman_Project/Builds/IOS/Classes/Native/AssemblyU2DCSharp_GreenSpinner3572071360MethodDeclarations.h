﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GreenSpinner
struct GreenSpinner_t3572071360;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"

// System.Void GreenSpinner::.ctor()
extern "C"  void GreenSpinner__ctor_m687158761 (GreenSpinner_t3572071360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreenSpinner::Start()
extern "C"  void GreenSpinner_Start_m3664346785 (GreenSpinner_t3572071360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreenSpinner::Update()
extern "C"  void GreenSpinner_Update_m1608625898 (GreenSpinner_t3572071360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreenSpinner::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void GreenSpinner_OnTriggerEnter2D_m2666232697 (GreenSpinner_t3572071360 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreenSpinner::MoveEnemy(System.Int32)
extern "C"  void GreenSpinner_MoveEnemy_m2147191535 (GreenSpinner_t3572071360 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GreenSpinner::Shoot()
extern "C"  void GreenSpinner_Shoot_m1844586608 (GreenSpinner_t3572071360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GreenSpinner::WaitShoot()
extern "C"  Il2CppObject * GreenSpinner_WaitShoot_m1483057635 (GreenSpinner_t3572071360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
