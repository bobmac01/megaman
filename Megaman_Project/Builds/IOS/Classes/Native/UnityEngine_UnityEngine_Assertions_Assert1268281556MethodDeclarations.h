﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Assertions.Assert::Fail(System.String,System.String)
extern "C"  void Assert_Fail_m1857243036 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___userMessage1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Assertions.Assert::IsTrue(System.Boolean,System.String)
extern "C"  void Assert_IsTrue_m2900904701 (Il2CppObject * __this /* static, unused */, bool ___condition0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
