﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyHealth
struct  EnemyHealth_t704393934  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 EnemyHealth::currentHealth
	int32_t ___currentHealth_2;
	// UnityEngine.GameObject EnemyHealth::healthDrop
	GameObject_t1756533147 * ___healthDrop_3;
	// UnityEngine.Transform EnemyHealth::myTrans
	Transform_t3275118058 * ___myTrans_4;
	// System.Int32 EnemyHealth::score
	int32_t ___score_5;

public:
	inline static int32_t get_offset_of_currentHealth_2() { return static_cast<int32_t>(offsetof(EnemyHealth_t704393934, ___currentHealth_2)); }
	inline int32_t get_currentHealth_2() const { return ___currentHealth_2; }
	inline int32_t* get_address_of_currentHealth_2() { return &___currentHealth_2; }
	inline void set_currentHealth_2(int32_t value)
	{
		___currentHealth_2 = value;
	}

	inline static int32_t get_offset_of_healthDrop_3() { return static_cast<int32_t>(offsetof(EnemyHealth_t704393934, ___healthDrop_3)); }
	inline GameObject_t1756533147 * get_healthDrop_3() const { return ___healthDrop_3; }
	inline GameObject_t1756533147 ** get_address_of_healthDrop_3() { return &___healthDrop_3; }
	inline void set_healthDrop_3(GameObject_t1756533147 * value)
	{
		___healthDrop_3 = value;
		Il2CppCodeGenWriteBarrier(&___healthDrop_3, value);
	}

	inline static int32_t get_offset_of_myTrans_4() { return static_cast<int32_t>(offsetof(EnemyHealth_t704393934, ___myTrans_4)); }
	inline Transform_t3275118058 * get_myTrans_4() const { return ___myTrans_4; }
	inline Transform_t3275118058 ** get_address_of_myTrans_4() { return &___myTrans_4; }
	inline void set_myTrans_4(Transform_t3275118058 * value)
	{
		___myTrans_4 = value;
		Il2CppCodeGenWriteBarrier(&___myTrans_4, value);
	}

	inline static int32_t get_offset_of_score_5() { return static_cast<int32_t>(offsetof(EnemyHealth_t704393934, ___score_5)); }
	inline int32_t get_score_5() const { return ___score_5; }
	inline int32_t* get_address_of_score_5() { return &___score_5; }
	inline void set_score_5(int32_t value)
	{
		___score_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
