﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MovingPlatform
struct MovingPlatform_t2706464437;
// UnityEngine.Collider2D
struct Collider2D_t646061738;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"

// System.Void MovingPlatform::.ctor()
extern "C"  void MovingPlatform__ctor_m961234664 (MovingPlatform_t2706464437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingPlatform::Start()
extern "C"  void MovingPlatform_Start_m578698656 (MovingPlatform_t2706464437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingPlatform::Update()
extern "C"  void MovingPlatform_Update_m3559182657 (MovingPlatform_t2706464437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingPlatform::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void MovingPlatform_OnTriggerEnter2D_m2041124968 (MovingPlatform_t2706464437 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingPlatform::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void MovingPlatform_OnTriggerExit2D_m397890506 (MovingPlatform_t2706464437 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingPlatform::MovePlatform(System.Int32)
extern "C"  void MovingPlatform_MovePlatform_m3521879507 (MovingPlatform_t2706464437 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
