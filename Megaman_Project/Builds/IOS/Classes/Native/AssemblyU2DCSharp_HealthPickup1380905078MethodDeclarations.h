﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HealthPickup
struct HealthPickup_t1380905078;
// UnityEngine.Collider2D
struct Collider2D_t646061738;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"

// System.Void HealthPickup::.ctor()
extern "C"  void HealthPickup__ctor_m3353756487 (HealthPickup_t1380905078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HealthPickup::Start()
extern "C"  void HealthPickup_Start_m2357248323 (HealthPickup_t1380905078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HealthPickup::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void HealthPickup_OnTriggerEnter2D_m4026101659 (HealthPickup_t1380905078 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
