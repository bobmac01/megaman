﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	/*
	 * This is for any standard enemy.
	 * Each enemy consists of:
	 * Animator
	 * GroundCheck(2D Box Collider)
	 * This script
	 * Animation between the two or three states
	 */

	// Gameobject specific
	Rigidbody2D rb;
	Transform enemyTrans;
	Animator myAnim;

	// Public variables
	[SerializeField]
	private float moveSpeed;
	[SerializeField]
	private float jumpSpeed;
	[SerializeField]
	private float health;
	[SerializeField]
	private float seconds;
	[SerializeField]
	private int enemydefect;

	// Not shown for the inspector
	private bool moveRight;

	// Ground check variables
	public LayerMask enemyMask;
	bool isGrounded;
	Transform groundTag;



	#region Unity specific methods
	void Start () 
	{
		// Getting the enemy components
		enemyTrans = this.transform;
		rb = this.GetComponent<Rigidbody2D> ();
		myAnim = this.GetComponent<Animator> ();

		// Setting the ground object for the line check
		groundTag = GameObject.Find (this.name + "/GroundCheck").transform;

		// Each enemy can pass each other
		Physics2D.IgnoreLayerCollision (10, 10);

		//Start the WaitForSeconds();
		StartCoroutine (JumpingLogic ());
	}
	
	void Update () 
	{
		if(this.gameObject != null)
		{
			// Linecast to check if the enemy is grounded or not
			isGrounded = Physics2D.Linecast (enemyTrans.position, groundTag.position, enemyMask);
		} else{
			Debug.Log("Enemy is dead");
		}



		if (isGrounded) 
		{
			// Setting animation bool
			myAnim.SetBool ("Jumping", false);
		}

		// if moveRight is set true move left else move right
		if (moveRight) 
		{
			rb.velocity = new Vector2 (moveSpeed, rb.velocity.y);
			enemyTrans.localScale = new Vector2 (-10f, 10);
		} 
		else 
		{
			rb.velocity = new Vector2 (-moveSpeed, rb.velocity.y);
			enemyTrans.localScale = new Vector2 (10f, 10);
		}
	}
	#endregion

	#region Trigger traps
	void OnTriggerEnter2D(Collider2D other) 
	{
		// Checks for when the player enters a specific GameObject
		if (other.gameObject.tag == "RightPoint") 
		{
			moveRight = false;
		}

		if (other.gameObject.tag == "LeftPoint") 
		{
			moveRight = true;
		}

		if (other.gameObject.tag == "Player") 
		{
			var hit = other.gameObject;
			var playerHealth = hit.GetComponent<PlayerHealth> ();

			if (playerHealth != null) 
			{
				playerHealth.TakeDamage (enemydefect);
			}
		}

	}

	void OnTriggerStay2D(Collider2D other)
	{
		// Works the same way as when a player is in spikes
		if (other.gameObject.tag == "Player") 
		{
			var hit = other.gameObject;
			var playerHealth = hit.GetComponent<PlayerHealth> ();

			if (playerHealth != null) 
			{
				playerHealth.PlayerDrown ();
			}
		}
	}
	#endregion

	#region custom methods
	public void MoveEnemy()
	{
		/*
		 * This is for any jumping enemy
		 * Set the speed and jump floats from the inspector
		 * for the desired bounce and movement
		 */

		rb.velocity = new Vector2 (moveSpeed, jumpSpeed);
		//Debug.Log ("Jumping");
		isGrounded = false;
		myAnim.SetBool ("Jumping", true);
	}

	IEnumerator JumpingLogic()
	{
		// Automatic wait for seconds
		// Once waited MoveEnemy() is called
		while (true) 
		{
			yield return new WaitForSeconds(seconds);
				MoveEnemy();
		}
	}
	#endregion
}
