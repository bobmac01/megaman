﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour 
{
	// Class for the health of the player.

	[SerializeField]
	private int maxHealth;
	public static int currentHealth = 100;

	void Update()
	{
		if (currentHealth < 1) 
		{
			GameManager.isDead = true;
			Debug.Log ("Player has died");
		}
	}

	public void TakeDamage(int amount)
	{
		// Remove amount passed from Enemy script
		currentHealth -= amount;
		if (currentHealth <= 0) 
		{
			// Player dies
			currentHealth = 0;
			Debug.Log ("Player is deed");
			Destroy (gameObject);
		}
	}

	public void HealthPickup(int toGive)
	{
		// Enemy dies so a health pickup is dropped
		currentHealth += toGive;

		if (currentHealth > maxHealth) 
		{
			// Does not let the current health override the max health
			currentHealth = maxHealth;
		}
	}

	public void PlayerDrown()
	{
		// Method is mainly for when the player is sitting in spikes
		// or 
		// is standing against an enemy
		currentHealth -= 1;
	}
}
