﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour 
{
	[SerializeField]
	private int healthToGive;

	private PlayerHealth pHealth;

	void Start () 
	{
		this.pHealth = FindObjectOfType<PlayerHealth> ();
		
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player") 
		{
			// Adds the health to player
			// Destroys health pickup game object
			pHealth.HealthPickup (healthToGive);
			Destroy (this.gameObject);
		}
	}
}
