﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenSpinnerShoot : MonoBehaviour {

	// Object specific variable
	Rigidbody2D rb;
	GreenSpinner spinner;

	// These are set from within inspector
	[SerializeField]
	private float shootSpeed;
	[SerializeField]
	private int bulletDamage;

	#region Unity specific methods
	void Start () 
	{
		this.rb = GetComponent<Rigidbody2D> ();
		this.spinner = FindObjectOfType<GreenSpinner> ();

		// If player is facing left
		// Shoot bullet minus speed so it goes left
		if (spinner.transform.localScale.x > 0) 
		{
			shootSpeed = -shootSpeed;
		}
	}

	void Update ()
	{
		rb.velocity = new Vector2 (shootSpeed, rb.velocity.y);
	}
	#endregion

	#region trigger traps
	public void OnCollisionEnter2D(Collision2D other)
	{
		var hit = other.gameObject;
		var playerHealth = hit.GetComponent<PlayerHealth> ();

		if (playerHealth != null) 
		{
			playerHealth.TakeDamage (bulletDamage);
		}

		Destroy (gameObject);
	}
	#endregion
}
