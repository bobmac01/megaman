﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour 
{
	/*
	 * Simple class to make use of the GUI navigation buttons
	 */

	public void NewGame()
	{
		SceneManager.LoadScene("StageSelect");
	}

	public void Stage1()
	{
		SceneManager.LoadScene("Dr Wily Stage 1");
	}

	public void Stage2()
	{
		SceneManager.LoadScene("Gutsman Stage");
	}

	public void ShowControls()
	{
		SceneManager.LoadScene ("ControlsScene");
	}

	public void ShowLegal()
	{
		SceneManager.LoadScene ("LegalScene");
	}

	public void Back()
	{
		SceneManager.LoadScene ("MainMenu");
	}

	public void MainMenu()
	{
		SceneManager.LoadScene ("MainMenu");
	}
}
