﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenSpinner : MonoBehaviour {

	// This class is for the green spinner enemy

	// Gameobject specific
	Rigidbody2D rb;
	Transform enemyTrans;

	//Bullet
	public Transform firePoint;
	public GameObject bullet;

	// Public variables
	[SerializeField]
	private float moveSpeed;
	[SerializeField]
	private float seconds;

	private int directionVal;

	#region Unity specific methods
	void Start () 
	{
		// Getting the enemy components
		enemyTrans = this.transform;
		rb = this.GetComponent<Rigidbody2D> ();
		Physics2D.IgnoreLayerCollision (10, 10);

		StartCoroutine (WaitShoot ());
	}

	void Update () 
	{
		MoveEnemy (directionVal);
	}
	#endregion

	#region Trigger traps
	void OnTriggerEnter2D(Collider2D other) 
	{
		// Checks for when the player enters either
		// the left point or the right
		if (other.gameObject.tag == "RightPoint") 
		{
			directionVal = 0;
		}

		if (other.gameObject.tag == "LeftPoint") 
		{
			directionVal = 1;
		}
	}
	#endregion

	#region custom methods
	public void MoveEnemy(int direction)
	{
		// Gets the enemy direction from the passed in variable direction
		if (direction == 1) {
			rb.velocity = new Vector2 (moveSpeed, 0f);
			enemyTrans.localScale = new Vector2 (-12f, 12);
		} 
		else if (direction == 0) 
		{
			rb.velocity = new Vector2 (-moveSpeed, 0f);
			enemyTrans.localScale = new Vector2 (12f, 12);
		}
	}

	public void Shoot()
	{
		// Creates a bullet gameobject
		Instantiate (bullet, firePoint.position, firePoint.rotation);
	}

	IEnumerator WaitShoot()
	{
		// Automatic wait for seconds
		// Once waited Shoot() is called
		// Seconds of wait is changed per game object via the inspector
		while (true) 
		{
			yield return new WaitForSeconds(seconds);
			Shoot();
		}
	}
	#endregion
}
