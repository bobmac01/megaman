﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour 
{	
	// Class for the health of an enemy

	[SerializeField]
	private int currentHealth;

	public GameObject healthDrop;
	Transform myTrans;

	public int score;


	void Start () 
	{
		myTrans = this.transform;
	}

	public void TakeDamage(int amount)
	{
		// This is called from Shooting.cs
		// Similar method to how the PlayerHealth.cs method works
		if(currentHealth < 10)
		{
			Debug.Log("Death if statement");
			ScoreManager.score += score;
			Instantiate (healthDrop, myTrans.position, myTrans.rotation);
			ProcessEnemyDeath();
		}

		currentHealth -= amount;


	}

	void ProcessEnemyDeath()
	{
		Destroy(gameObject);
		Debug.Log("procssing enemy death");
	}
}
