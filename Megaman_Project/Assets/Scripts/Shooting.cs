﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour 
{
	// Object specific variable
	PlayerController player;
	Rigidbody2D rb;

	// These are private but are set in the inspector
	[SerializeField]
	private float shootSpeed = 10;
	[SerializeField]
	private int bulletDamage;

	#region Unity specific methods
	void Start () 
	{
		this.rb = GetComponent<Rigidbody2D> ();
		this.player = FindObjectOfType<PlayerController> ();

		// If player is facing left
		// Shoot bullet minus speed so it goes left
		if (player.transform.localScale.x < 0) 
		{
			shootSpeed = -shootSpeed;
		}
	}

	void Update ()
	{
		// Setting the direction and speed of the bullet
		rb.velocity = new Vector2 (shootSpeed, rb.velocity.y);
	}
	#endregion

	#region trigger traps
	public void OnCollisionEnter2D(Collision2D other)
	{
		// Gets the object that the bullet has collided with
		var hit = other.gameObject;
		var enemyHealth = hit.GetComponent<EnemyHealth> ();

		if (enemyHealth != null) 
		{
			// Removing health from enemy
			enemyHealth.TakeDamage (bulletDamage);
		}
		// Destroys enemy game object
		Destroy (this.gameObject);
	}
	#endregion
}
