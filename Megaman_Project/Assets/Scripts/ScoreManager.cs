﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour 
{
	/* 
	 * Class simply updates the GUI text field
	 * containing the current score
	*/
	public static int score;
	[SerializeField]
	Text scoreText;
	[SerializeField]
	Text healthText;

	void Awake()
	{
		// Setting the score to 0
		// This also works as a Reset
		//scoreText = GetComponent<Text> ();
		score = 0;
	}

	void Update()
	{
		// Setting the Text field
		scoreText.text = "Score: " + score;
		healthText.text = "Health: " + PlayerHealth.currentHealth;
	}
}
