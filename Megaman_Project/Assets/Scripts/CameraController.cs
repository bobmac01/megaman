﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public GameObject target;

	public bool bounds;
	public Vector3 minPos;
	public Vector3 maxPos;

	void Update ()
	{
		if (target == null) 
		{
			Debug.Log ("GM knows Player is now dead");
		} 
		else 
		{
			transform.position = new Vector3 (target.transform.position.x, target.transform.position.y + 3, transform.position.z);

			if (bounds) 
			{
				transform.position = new Vector3 (
					Mathf.Clamp (transform.position.x, minPos.x, maxPos.x),
					Mathf.Clamp (transform.position.y, minPos.y, maxPos.y),
					Mathf.Clamp (transform.position.z, minPos.z, maxPos.z));
			}
		}
	}
}
