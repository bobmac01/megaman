﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour 
{
	public GameObject playerObject;

	[SerializeField]
	public static bool isDead;
	public static bool levelCompleted;

	public int points;

	// Update is called once per frame
	void Update () 
	{
		if (playerObject == null) 
		{
			isDead = true;
			Debug.Log ("Player is now dead");
		}

		if (isDead) 
		{
			DestroyPlayer ();
		}

		if (levelCompleted) 
		{
			Debug.Log ("Level Completed");
			SceneManager.LoadScene ("GameOver");
		}
	}

	void DestroyPlayer()
	{
		// Player is killed
		Destroy (playerObject);
	}

}
