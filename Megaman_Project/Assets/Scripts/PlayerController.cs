﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour 
{
	// General Player attributes.
	// Made public so we can set them in the inspector
	[SerializeField]
	private int speed = 10;
	[SerializeField]
	private int jumpSpeed = 10;
	[SerializeField]
	private int climbSpeed = 5;

	private bool isShooting = false;

	// Player specific components
	Transform myTransform;
	Rigidbody2D rb;
	AudioSource shootSound;

	float hInput = 0;
	public LayerMask playerMask;

	// Reference from AnimatorControl.cs
	AnimatorControl myAnim;

	// Ground check
	private bool isGrounded = false;
	Transform groundTag;

	// Ladder Climb
	private bool canClimb;
	private bool isClimbing;

	// For Bullet
	public Transform firePoint;
	public GameObject bullet;

	void Start () 
	{
		// Instantiating the components
		rb = this.GetComponent<Rigidbody2D> ();
		myTransform = this.transform;

		// Finding the groundCheck gameobject on the player object
		groundTag = GameObject.Find (this.name + "/GroundCheck").transform;

		// Assigning the AnimatorControl.cs script
		myAnim = GetComponent<AnimatorControl> (); // This now works as myAnim is no longer null because of a false reference

		// Setting the shoot sound
		shootSound = GetComponent<AudioSource> ();
		/*
		 * myAnim = AnimatorControl.instance; - This doesn't work. Keep for the sake of the testing log.
		 */

	}

	void Update () 
	{
		// linecast to check when player is ground on anything - Updates bool too.
		isGrounded = Physics2D.Linecast (myTransform.position, groundTag.position, playerMask);
		myAnim.UpdateGrounded (isGrounded);
/* 
		// IF statement to check if build is for Android or PC
		// The if functions are greyed out if it's an Android build
		#if !UNITY_ANDROID

		hInput = Input.GetAxisRaw("Horizontal");
		myAnim.UpdateSpeed(hInput);

		if (Input.GetButtonDown ("Jump")) 
		{
			Jump ();
		}

		//Two if's for Player Shoot
		if(Input.GetButtonDown("Fire1"))
		{
			//User pushes shoot button and Shoot() is run
			Debug.Log("Shooting key pushed");
			Shoot();
		}

		if(Input.GetButtonUp("Fire1"))
		{
			//User lifts off shoot button and sets isShooting to false
			isShooting = false;
			Debug.Log("no shooting now");
			myAnim.UpdateShoot(isShooting);
		}

		if(Input.GetAxisRaw("Vertical") > 0 && canClimb)
		{
			Debug.Log("Up key pushed");
			myAnim.UpdateClimbing(true);
			Climb();
		}
		#endif
*/

		// Does this when it's an Android build
		Move (hInput);

		if(canClimb)
		{
			if (isClimbing) 
			{
				Climb ();
			}
		}
	}

	#region Trigger traps
	void OnTriggerEnter2D(Collider2D other) 
	{
		// Checks for when the player enters a specific GameObject
		if (other.gameObject.name == "Ladder") 
		{
			canClimb = true;
		}

		if (other.gameObject.name == "PlayerLimitLeft") 
		{
			rb.velocity = new Vector2 (0f, rb.velocity.y);
		}

		if (other.gameObject.name == "PlayerFinishPoint") 
		{
			// Telling the GameManager the level is finished
			GameManager.levelCompleted = true;
		}
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if (other.gameObject.name == "Spikes") 
		{
			GetComponent<PlayerHealth> ().PlayerDrown ();
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		// Checks for when the player exits a specific GameObject
		if (other.gameObject.name == "Ladder") 
		{
			canClimb = false;
			myAnim.UpdateClimbing (false);
		}
	}
	#endregion

	#region Android control
	public void Move(float horizontal)
	{
		// Self explanatory. Moves the player left and right
		Vector2 moveVelocity = rb.velocity;
		moveVelocity.x = horizontal * speed;
		rb.velocity = moveVelocity;
	}

	public void Jump()
	{
		// Let's the player jump if the grounded bool is true. 
		// This stops the double or inifite jumping

		if (isGrounded) 
		{
			//rb.velocity += jumpSpeed * Vector2.up;
			rb.velocity = new Vector2(0f, jumpSpeed);
		}
	}

	public void Climb()
	{
		myAnim.UpdateClimbing(true);
		isClimbing = true;
		rb.velocity = new Vector2 (0f, climbSpeed);
	}

	public void StartClimb(float inputVal)
	{
		if (inputVal == 1) {
			isClimbing = true;
		} else 
		{
			isClimbing = false;
		}
	}

	public void Shoot()
	{
		isShooting = true;
		myAnim.UpdateShoot (isShooting);
		Handheld.Vibrate ();
		shootSound.Play ();
		Instantiate (bullet, firePoint.position, firePoint.rotation);
	}

	public void StartMove(float horizontal)
	{
		// Called when the user moves left or right by interacting with the on-screen controls
		// Main use is for the touch screen controls

		hInput = horizontal;
		myAnim.UpdateSpeed(horizontal);
	}

	public void StopShoot(bool shoot)
	{
		// Called when the user removes their finger from the Shoot button
		// Main use is for the touch screen controls
		isShooting = shoot;
		myAnim.UpdateShoot (isShooting);
	}
	#endregion
}
