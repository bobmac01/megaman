﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Timer : MonoBehaviour 
{
	public float timeLeft;
	public Text timerText;

	void Update () 
	{
		timeLeft -= Time.deltaTime;
		timerText.text = "Time left: " + Mathf.Round (timeLeft);

		if (timeLeft < 0) 
		{
			SceneManager.LoadScene ("GameOver");
		}
	}
}
