﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorControl : MonoBehaviour 
{
	// Instance to access from PlayerController.cs
	public static AnimatorControl instance;

	// General Player components
	Transform myTransformAnim;
	Animator myAnimAnimator;
	Vector3 artScale;

	// Use this for initialization
	void Start () 
	{
		myTransformAnim = this.transform;
		myAnimAnimator = this.gameObject.GetComponent<Animator> ();
		artScale = myTransformAnim.localScale;
		instance = this;
	}

	public void FlipPlayer(float currentspeed)
	{
		/* To flip the player sprite.
		 * If player is facing left but going right and vise versa
		 */
		if((currentspeed < 0 && artScale.x == 8) || 
			(currentspeed > 0 && artScale.x == -8))
		{
			artScale.x *= -1;
			myTransformAnim.localScale = artScale;
		}
	}

	public void UpdateSpeed(float currentspeed)
	{
		// Updates the speed variable in Animator and called in PlayerController.cs
		myAnimAnimator.SetFloat ("Speed", currentspeed);
		FlipPlayer (currentspeed);
	}

	public void UpdateGrounded(bool ground)
	{
		// Updates the grounded variable in Animator and called in PlayerController.cs
		myAnimAnimator.SetBool ("Grounded", ground);
	}

	public void UpdateClimbing(bool climb)
	{
		// Updating the climbing animation
		myAnimAnimator.SetBool ("Climb", climb);
	}

	public void UpdateShoot(bool shoot)
	{
		// Updating the shooting animation
		myAnimAnimator.SetBool ("Shooting", shoot);
	}
}
