﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour 
{

	Rigidbody2D rb;
	Animator myAnim;

	// Private Variables
	public float moveSpeed;
	public int directionVal;	

	void Start () 
	{
		this.rb = GetComponent<Rigidbody2D>();
		myAnim = this.gameObject.GetComponent<Animator>();
	}
	
	void Update () 
	{
		MovePlatform(directionVal);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "FinishPoint") 
		{
			directionVal = 0;
		}

		if (other.gameObject.tag == "StartPoint") 
		{
			directionVal = 1;
		}

		if(other.gameObject.tag == "Platform_Fold")
		{
			Debug.Log("Entered the fold trigger");
			myAnim.SetBool("Fold", true);
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if(other.gameObject.tag == "Platform_Fold")
		{
			Debug.Log("Left the fold trigger");
			myAnim.SetBool("Fold", false);
		}
	}

	public void MovePlatform(int direction)
	{
		// Gets the enemy direction from the passed in variable direction
		if (direction == 1) 
		{
			rb.velocity = new Vector2 (moveSpeed, 0f);
		} 
		else
		{
			rb.velocity = new Vector2 (-moveSpeed, 0f);
		}
	}


}
