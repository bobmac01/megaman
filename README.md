# README #

This README would normally document whatever steps are necessary to get your application up and running.

### This is my 4th year honours project ###

* What is it? Android remake of the NES classic: Megaman
* Version: 1.0

### How do I get set up? ###

* As long as you have Unity installed just clone this whole repo to get started.
* Click on Level1 found the sub folder of Assets called Scenes
* To play either build and run for the device in Unity or playing using the Game editor screen and the touch screen controls.
* You can change the build to PC standalone and use the keyboard keys
### Keyboard Keys ###
* Move - LEFT & RIGHT directional keys
* Climb Ladder - UP directional key
* Jump - SPACE Key
* Shoot - CTRL Key

### On Screen Controls ###
* Move - Directional left and right
* Climb Ladder - Directional Up
* Shoot - B Button
* Jump - A Button

### Future Versions ###
Currently, I am not looking to continue this project. In the next coming months I will be looking to build a new game from the ground up, adding in features I have tried to do so here.
The structure of the game is not great and there are many improvements to be made. 